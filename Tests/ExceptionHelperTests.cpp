#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"

#include <string>

TEST(ExceptionHelperTests, AssertEquals)
{
    const std::string err{"Must be equal"};

    EXPECT_NO_THROW(utils::AssertEquals(1, 1, err));
    EXPECT_THROW(utils::AssertEquals(1, 2, err), std::runtime_error);

    EXPECT_NO_THROW(utils::AssertEquals(1.0, 1.0, err));
    EXPECT_THROW(utils::AssertEquals(1.0, 1.1, err), std::runtime_error);

    using namespace std::string_literals;
    EXPECT_NO_THROW(utils::AssertEquals("alma"s, "alma"s, err));
    EXPECT_THROW(utils::AssertEquals("repa"s, "retek"s, err), std::runtime_error);
}