#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"

using namespace utils;

namespace
{
	class TrafficLight : public StringEnum
	{
	public:
		static const TrafficLight Red;
		static const TrafficLight Yellow;
		static const TrafficLight Green;

	protected:
		explicit TrafficLight(const String& enumVal) : StringEnum(enumVal)
		{
		}
	};

	inline const TrafficLight TrafficLight::Red{ STR("Red") };
	inline const TrafficLight TrafficLight::Yellow{ STR("Yellow") };
	inline const TrafficLight TrafficLight::Green{ STR("Green") };
}

TEST(StringEnumOperatorsTests, LessThan)
{
	EXPECT_LT(TrafficLight::Red, TrafficLight::Yellow);
	EXPECT_LT(TrafficLight::Green, TrafficLight::Red);
	EXPECT_LT(TrafficLight::Green, TrafficLight::Yellow);
	EXPECT_FALSE(TrafficLight::Red < TrafficLight::Red);
}