#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"

using utils::DateTime;

namespace
{
	constexpr time_t TIMESTAMP = 1637399687;	//Sat, 20 Nov 2021 09:14:47 GMT
}

TEST(DateTimeTests, FromUnixTimestamp)
{
	DateTime dt = DateTime::FromUnixTimestamp(TIMESTAMP);
	EXPECT_EQ(2021, dt.year);
	EXPECT_EQ(11, dt.month);
	EXPECT_EQ(20, dt.day);
	EXPECT_EQ(9, dt.hour);
	EXPECT_EQ(14, dt.minute);
	EXPECT_EQ(47, dt.second);
}

TEST(DateTimeTests, ToUnixTimestamp)
{
	DateTime dt;
	dt.year = 2021;
	dt.month = 11;
	dt.day = 20;
	dt.hour = 9;
	dt.minute = 14;
	dt.second = 47;

	EXPECT_EQ(TIMESTAMP, dt.ToUnixTimestamp());
}