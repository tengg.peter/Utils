#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"
//std
#include <sstream>

TEST(StringConversionTests, PercentToDouble)
{
	const double val = 16.28;

	const std::string s{ "16.28" };
	const std::string sp{ "16.28%" };
	const std::wstring ws{ L"16.28" };
	const std::wstring wsp{ L"16.28%" };

	EXPECT_EQ(val, utils::string_conversion::PercentToDouble(s));
	EXPECT_EQ(val, utils::string_conversion::PercentToDouble(sp));
	EXPECT_EQ(val, utils::string_conversion::PercentToDouble(ws));
	EXPECT_EQ(val, utils::string_conversion::PercentToDouble(wsp));
}

const std::string s1{ "Sensodyne" };
const std::wstring ws1{ L"Sensodyne" };
const utils::String us1{ STR("Sensodyne") };

TEST(StringConversionTests, ConvertStdString)
{
	EXPECT_EQ(ws1, utils::string_conversion::StdStringToWstring(s1));
	EXPECT_EQ(us1, utils::string_conversion::StdStringToUtilsString(s1));
	EXPECT_EQ(us1, utils::ToWstring(s1));
}

TEST(StringConversionTests, ConvertWString)
{
	EXPECT_EQ(s1, utils::string_conversion::WstringToStdString(ws1));
	EXPECT_EQ(s1, utils::ToStdString(ws1));
}

TEST(StringConversionTests, ConvertUtilsString)
{
	EXPECT_EQ(s1, utils::string_conversion::UtilsStringToStdString(us1));
	EXPECT_EQ(s1, utils::ToStdString(us1));

	EXPECT_EQ(ws1, utils::string_conversion::UtilsStringToWstring(us1));
	EXPECT_EQ(ws1, utils::ToWstring(us1));
}
