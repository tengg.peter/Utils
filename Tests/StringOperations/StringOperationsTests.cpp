#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"
//std
#include <sstream>

TEST(StringOperationsTests, Split)
{
	std::string s{
		"CREATE TABLE continents(		\n"
		"	id INTEGER PRIMARY KEY,		\n"
		"	sm_id INTEGER UNIQUE,		\n"
		"	name TEXT NOT NULL UNIQUE	\n"
		");								\n" };

	std::wstring ws{
		L"CREATE TABLE continents(		\n"
		"	id INTEGER PRIMARY KEY,		\n"
		"	sm_id INTEGER UNIQUE,		\n"
		"	name TEXT NOT NULL UNIQUE	\n"
		");								\n" };

	const std::vector<std::string> delims{ " ", "\t", "\n" };
	const std::vector<std::wstring> wDelims{ L" ", L"\t", L"\n" };

	//char
	std::vector<std::string> notSkipEmptyNotIncludeDelims = utils::string_operations::SplitByEach(s, delims, false, false);
	EXPECT_EQ(35, notSkipEmptyNotIncludeDelims.size());
	EXPECT_EQ("CREATE", notSkipEmptyNotIncludeDelims.front());
	EXPECT_EQ("", notSkipEmptyNotIncludeDelims.back());

	std::vector<std::string> notSkipEmptyIncludeDelims = utils::string_operations::SplitByEach(s, delims, false, true);
	EXPECT_EQ(69, notSkipEmptyIncludeDelims.size());
	EXPECT_EQ("CREATE", notSkipEmptyIncludeDelims.front());
	EXPECT_EQ("", notSkipEmptyIncludeDelims.back());

	std::vector<std::string> skipEmptyNotIncludeDelims = utils::string_operations::SplitByEach(s, delims, true, false);
	EXPECT_EQ(16, skipEmptyNotIncludeDelims.size());
	EXPECT_EQ("CREATE", skipEmptyNotIncludeDelims.front());
	EXPECT_EQ(");", skipEmptyNotIncludeDelims.back());

	std::vector<std::string> skipEmptyIncludeDelims = utils::string_operations::SplitByEach(s, delims, true, true);
	EXPECT_EQ(50, skipEmptyIncludeDelims.size());
	EXPECT_EQ("CREATE", skipEmptyIncludeDelims.front());
	EXPECT_EQ("\n", skipEmptyIncludeDelims.back());

	//wchar
	std::vector<std::wstring> wNotSkipEmptyNotIncludeDelims = utils::string_operations::SplitByEach(ws, wDelims, false, false);
	EXPECT_EQ(35, wNotSkipEmptyNotIncludeDelims.size());
	EXPECT_EQ(L"CREATE", wNotSkipEmptyNotIncludeDelims.front());
	EXPECT_EQ(L"", wNotSkipEmptyNotIncludeDelims.back());

	std::vector<std::wstring> wNotSkipEmptyIncludeDelims = utils::string_operations::SplitByEach(ws, wDelims, false, true);
	EXPECT_EQ(69, wNotSkipEmptyIncludeDelims.size());
	EXPECT_EQ(L"CREATE", wNotSkipEmptyIncludeDelims.front());
	EXPECT_EQ(L"", wNotSkipEmptyIncludeDelims.back());

	std::vector<std::wstring> wSkipEmptyNotIncludeDelims = utils::string_operations::SplitByEach(ws, wDelims, true, false);
	EXPECT_EQ(16, wSkipEmptyNotIncludeDelims.size());
	EXPECT_EQ(L"CREATE", wSkipEmptyNotIncludeDelims.front());
	EXPECT_EQ(L");", wSkipEmptyNotIncludeDelims.back());

	std::vector<std::wstring> wSkipEmptyIncludeDelims = utils::string_operations::SplitByEach(ws, wDelims, true, true);
	EXPECT_EQ(50, wSkipEmptyIncludeDelims.size());
	EXPECT_EQ(L"CREATE", wSkipEmptyIncludeDelims.front());
	EXPECT_EQ(L"\n", wSkipEmptyIncludeDelims.back());
}

TEST(StringOperationsTests, TokeniseSql)
{
	std::string sql{
		"CREATE TABLE leagues(											"
		"	id INTEGER PRIMARY KEY,										"
		"	sm_id INTEGER NOT NULL UNIQUE,								"
		"	active BOOLEAN NOT NULL,									"
		"	type TEXT,													"
		"	sm_legacy_id INTEGER NOT NULL UNIQUE,						"
		"	sm_country_id INTEGER NOT NULL,								"
		"	logo_path TEXT,												"
		"	name TEXT NOT NULL,											"
		"	is_cup BOOLEAN NOT NULL,									"
		"	sm_current_season_id INTEGER NOT NULL,						"
		"	sm_current_round_id INTEGER,								"
		"	sm_current_stage_id INTEGER,								"
		"	live_standings BOOLEAN NOT NULL,							"
		"	coverage_predictions BOOLEAN NOT NULL,						"
		"	coverage_topscorer_goals BOOLEAN NOT NULL,					"
		"	coverage_topscorer_assists BOOLEAN NOT NULL,				"
		"	coverage_topscorer_cards BOOLEAN NOT NULL,					"
		"																"
		"	FOREIGN KEY(sm_country_id) REFERENCES countries(sm_id),		"
		"	FOREIGN KEY(sm_current_season_id) REFERENCES seasons(sm_id),"
		"	FOREIGN KEY(sm_current_round_id) REFERENCES rounds(sm_id),	"
		"	FOREIGN KEY(sm_current_stage_id) REFERENCES stage(sm_id),	"
		"	UNIQUE (sm_country_id, name)								"
		");																" };

	std::wstring wSql{
		L"CREATE TABLE leagues(											"
		"	id INTEGER PRIMARY KEY,										"
		"	sm_id INTEGER NOT NULL UNIQUE,								"
		"	active BOOLEAN NOT NULL,									"
		"	type TEXT,													"
		"	sm_legacy_id INTEGER NOT NULL UNIQUE,						"
		"	sm_country_id INTEGER NOT NULL,								"
		"	logo_path TEXT,												"
		"	name TEXT NOT NULL,											"
		"	is_cup BOOLEAN NOT NULL,									"
		"	sm_current_season_id INTEGER NOT NULL,						"
		"	sm_current_round_id INTEGER,								"
		"	sm_current_stage_id INTEGER,								"
		"	live_standings BOOLEAN NOT NULL,							"
		"	coverage_predictions BOOLEAN NOT NULL,						"
		"	coverage_topscorer_goals BOOLEAN NOT NULL,					"
		"	coverage_topscorer_assists BOOLEAN NOT NULL,				"
		"	coverage_topscorer_cards BOOLEAN NOT NULL,					"
		"																"
		"	FOREIGN KEY(sm_country_id) REFERENCES countries(sm_id),		"
		"	FOREIGN KEY(sm_current_season_id) REFERENCES seasons(sm_id),"
		"	FOREIGN KEY(sm_current_round_id) REFERENCES rounds(sm_id),	"
		"	FOREIGN KEY(sm_current_stage_id) REFERENCES stage(sm_id),	"
		"	UNIQUE (sm_country_id, name)								"
		");																" };

	std::vector<std::string> whiteSpaces{ " ", "\t", "\n" };
	std::vector<std::wstring> wWhiteSpaces{ L" ", L"\t", L"\n" };
	std::vector<std::string> specialCharacters{ "(", ")", ",", ";" };
	std::vector<std::wstring> wSpecialCharacters{ L"(", L")", L",", L";" };

	using utils::string_operations::SplitByEach;
	std::vector<std::string> splitByWhiteSpaces = SplitByEach(sql, whiteSpaces, true, false);
	std::vector<std::string> tokenisedSql;
	for (const auto& splitGroup : splitByWhiteSpaces)
	{
		std::vector<std::string> tokens = SplitByEach(splitGroup, specialCharacters, true, true);
		tokenisedSql.insert(tokenisedSql.end(), tokens.cbegin(), tokens.cend());
	}
	EXPECT_EQ(135, tokenisedSql.size());
	EXPECT_EQ("CREATE", tokenisedSql.front());
	EXPECT_EQ(")", tokenisedSql[133]);
	EXPECT_EQ(";", tokenisedSql.back());

	//wchar
	std::vector<std::wstring> wSplitByWhiteSpaces = SplitByEach(wSql, wWhiteSpaces, true, false);
	std::vector<std::wstring> wTokenisedSql;
	for (const auto& wSplitGroup : wSplitByWhiteSpaces)
	{
		std::vector<std::wstring> wTokens = SplitByEach(wSplitGroup, wSpecialCharacters, true, true);
		wTokenisedSql.insert(wTokenisedSql.end(), wTokens.cbegin(), wTokens.cend());
	}
	EXPECT_EQ(135, wTokenisedSql.size());
	EXPECT_EQ(L"CREATE", wTokenisedSql.front());
	EXPECT_EQ(L")", wTokenisedSql[133]);
	EXPECT_EQ(L";", wTokenisedSql.back());
}

TEST(StringOperationsTests, ToLower)
{
	const std::string input{ "iNpUt StRiNg" };
	const std::wstring wInput{ L"iNpUt WsTrInG" };

	EXPECT_EQ("input string", utils::string_operations::ToLower(input));
	EXPECT_EQ(L"input wstring", utils::string_operations::ToLower(wInput));
	EXPECT_EQ("input string", utils::ToLower(input));
	EXPECT_EQ(L"input wstring", utils::ToLower(wInput));
}

TEST(StringOperationsTests, ToUpper)
{
	const std::string input{ "iNpUt StRiNg" };
	const std::wstring wInput{ L"iNpUt WsTrInG" };

	EXPECT_EQ("INPUT STRING", utils::string_operations::ToUpper(input));
	EXPECT_EQ(L"INPUT WSTRING", utils::string_operations::ToUpper(wInput));
	EXPECT_EQ("INPUT STRING", utils::ToUpper(input));
	EXPECT_EQ(L"INPUT WSTRING", utils::ToUpper(wInput));
}

TEST(StringOperationsTests, ToLowerFirstChar)
{
	using utils::string_operations::ToLowerFirstChar;

	const std::string input{ "INpUt StRiNg" };
	const std::wstring wInput{ L"INpUt WsTrInG" };

	EXPECT_EQ("iNpUt StRiNg", ToLowerFirstChar(input));
	EXPECT_EQ(L"iNpUt WsTrInG", ToLowerFirstChar(wInput));
}

TEST(StringOperationsTests, ToUpperFirstChar)
{
	using utils::string_operations::ToUpperFirstChar;

	const std::string input{ "iNpUt StRiNg" };
	const std::wstring wInput{ L"iNpUt WsTrInG" };

	EXPECT_EQ("INpUt StRiNg", ToUpperFirstChar(input));
	EXPECT_EQ(L"INpUt WsTrInG", ToUpperFirstChar(wInput));
}

TEST(StringOperationsTests, TabTests)
{
	using utils::string_operations::Tab;

	Tab tab;
	std::stringstream ss;
	ss << tab << "banana";
	EXPECT_EQ("\tbanana", ss.str());

	ss.str("");
	ss << ++tab << "banana";
	EXPECT_EQ("\t\tbanana", ss.str());

	ss.str("");
	ss << --tab << "banana";
	EXPECT_EQ("\tbanana", ss.str());

	Tab tab2(4);
	ss.str("");
	ss << tab2 << "banana";
	EXPECT_EQ("\t\t\t\tbanana", ss.str());

	ss.str("");
	ss << ++tab2 << "banana";
	EXPECT_EQ("\t\t\t\t\tbanana", ss.str());

	ss.str("");
	ss << ----tab2 << "banana";
	EXPECT_EQ("\t\t\tbanana", ss.str());
}

TEST(StringOperationsTests, JoinStrings_char)
{
	using utils::string_operations::JoinStrings;

	const std::vector<std::string> abcdef{ "a","b","c","d","e","f" };

	{
		const std::string join = JoinStrings(abcdef, "->");
		EXPECT_EQ("a->b->c->d->e->f", join);
	}
	{
		const std::set<std::string> strings{ abcdef.crbegin(), abcdef.crend() };
		const std::string join = JoinStrings(strings, " <- ");
		EXPECT_EQ("a <- b <- c <- d <- e <- f", join);
	}
	{
		using namespace std::string_literals;
		{
			const std::string join = JoinStrings(abcdef, "->"s);
			EXPECT_EQ("a->b->c->d->e->f", join);
		}
		{
			const std::set<std::string> strings{ abcdef.crbegin(), abcdef.crend() };
			const std::string join = JoinStrings(strings, " <- "s);
			EXPECT_EQ("a <- b <- c <- d <- e <- f", join);
		}
	}
	{
		const std::vector<std::string> strings{ "","","","","","" };
		const std::string join = JoinStrings(strings, "-");
		EXPECT_EQ("-----", join);
	}
	{
		const std::vector<std::string> strings{ "1","2","3","4","5","6" };
		const std::string join = JoinStrings(strings, "");
		EXPECT_EQ("123456", join);
	}
	{
		const std::vector<std::string> strings{ "","","","","","" };
		const std::string join = JoinStrings(strings, "");
		EXPECT_EQ("", join);
	}
	{
		const std::vector<std::string> strings{};
		const std::string join = JoinStrings(strings, "X");
		EXPECT_EQ("", join);
	}
	{
		const std::vector<std::string> strings{};
		const std::string join = JoinStrings(strings, "");
		EXPECT_EQ("", join);
	}
}

TEST(StringOperationsTests, JoinStrings_wchar)
{
	using utils::string_operations::JoinStrings;

	const std::vector<std::wstring> abcdef{ L"a",L"b",L"c",L"d",L"e",L"f" };

	{
		const std::wstring join = JoinStrings(abcdef, L"->");
		EXPECT_EQ(L"a->b->c->d->e->f", join);
	}
	{
		const std::set<std::wstring> strings{ abcdef.crbegin(), abcdef.crend() };
		const std::wstring join = JoinStrings(strings, L" <- ");
		EXPECT_EQ(L"a <- b <- c <- d <- e <- f", join);
	}
	{
		using namespace std::string_literals;
		{
			const std::wstring join = JoinStrings(abcdef, L"->"s);
			EXPECT_EQ(L"a->b->c->d->e->f", join);
		}
		{
			const std::set<std::wstring> strings{ abcdef.crbegin(), abcdef.crend() };
			const std::wstring join = JoinStrings(strings, L" <- "s);
			EXPECT_EQ(L"a <- b <- c <- d <- e <- f", join);
		}
	}
	{
		const std::vector<std::wstring> strings{ L"",L"",L"",L"",L"",L"" };
		const std::wstring join = JoinStrings(strings, L"-");
		EXPECT_EQ(L"-----", join);
	}
	{
		const std::vector<std::wstring> strings{ L"1",L"2",L"3",L"4",L"5",L"6" };
		const std::wstring join = JoinStrings(strings, L"");
		EXPECT_EQ(L"123456", join);
	}
	{
		const std::vector<std::wstring> strings{ L"",L"",L"",L"",L"",L"" };
		const std::wstring join = JoinStrings(strings, L"");
		EXPECT_EQ(L"", join);
	}
	{
		const std::vector<std::wstring> strings{};
		const std::wstring join = JoinStrings(strings, L"X");
		EXPECT_EQ(L"", join);
	}
	{
		const std::vector<std::wstring> strings{};
		const std::wstring join = JoinStrings(strings, L"");
		EXPECT_EQ(L"", join);
	}

}

TEST(StringOperationsTests, Commas)
{
	using utils::Commas;

	EXPECT_EQ(STR("1"), Commas(1));
	EXPECT_EQ(STR("12"), Commas(12));
	EXPECT_EQ(STR("123"), Commas(123));
	EXPECT_EQ(STR("1,234"), Commas(1234));
	EXPECT_EQ(STR("12,345"), Commas(12345));
	EXPECT_EQ(STR("123,456"), Commas(123456));
	EXPECT_EQ(STR("1,234,567"), Commas(1234567));
	EXPECT_EQ(STR("12,345,678"), Commas(12345678));
	EXPECT_EQ(STR("123,456,789"), Commas(123456789));
	EXPECT_EQ(STR("1,234,567,890"), Commas(1234567890));
}