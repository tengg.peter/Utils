#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"
//std

using utils::Fmt;

TEST(FormatTests, Format_StdString)
{
	const std::string expected{ "kocka bucka" };

	EXPECT_EQ(expected, Fmt("kocka {}", "bucka"));

	const std::string formatArg{ "bucka" };
	EXPECT_EQ(expected, Fmt("kocka {}", formatArg));

	const std::string formatString{ "kocka {}" };
	EXPECT_EQ(expected, Fmt(formatString, "bucka"));

	EXPECT_EQ(expected, Fmt(formatString, formatArg));
}

TEST(FormatTests, Format_WString)
{
	const std::wstring expected{ L"kocka bucka" };

	EXPECT_EQ(expected, Fmt(L"kocka {}", L"bucka"));

	const std::wstring formatArg{ L"bucka" };
	EXPECT_EQ(expected, Fmt(L"kocka {}", formatArg));

	const std::wstring formatString{ L"kocka {}" };
	EXPECT_EQ(expected, Fmt(formatString, L"bucka"));

	EXPECT_EQ(expected, Fmt(formatString, formatArg));
}

TEST(FormatTests, Format_StdString_VariousArgs)
{
	const std::string expected{ "1.0, 2, tres" };

	EXPECT_EQ(expected, Fmt("{2:.1f}, {0}, {1}", 2, "tres", 1.0f));

	const int arg0 = 2;
	const char* arg1 = "tres";
	const float arg2 = 1.0f;
	EXPECT_EQ(expected, Fmt("{2:.1f}, {0}, {1}", arg0, arg1, arg2));

	const std::string formatString{ "{2:.1f}, {0}, {1}" };
	EXPECT_EQ(expected, Fmt(formatString, 2, "tres", 1.0f));

	EXPECT_EQ(expected, Fmt(formatString, arg0, arg1, arg2));
}

TEST(FormatTests, Format_WString_VariousArgs)
{
	const std::wstring expected{ L"1.0, 2, tres" };

	EXPECT_EQ(expected, Fmt(L"{2:.1f}, {0}, {1}", 2, L"tres", 1.0f));

	const int arg0 = 2;
	const wchar_t* arg1 = L"tres";
	const float arg2 = 1.0f;
	EXPECT_EQ(expected, Fmt(L"{2:.1f}, {0}, {1}", arg0, arg1, arg2));

	const std::wstring formatString{ L"{2:.1f}, {0}, {1}" };
	EXPECT_EQ(expected, Fmt(formatString, 2, L"tres", 1.0f));

	EXPECT_EQ(expected, Fmt(formatString, arg0, arg1, arg2));
}

TEST(FormatTests, DateTimeFormatter)
{
	const utils::DateTime dt(2022, 05, 21, 13, 14, 20);

	EXPECT_EQ("2022.05.21 13.14.20", utils::Fmt("{}", dt));

	const std::string format{ "{}" };
	EXPECT_EQ("2022.05.21 13.14.20", utils::Fmt(format, dt));

	EXPECT_EQ(L"2022.05.21 13.14.20", utils::Fmt(L"{}", dt));

	const std::wstring wformat{ L"{}" };
	EXPECT_EQ(L"2022.05.21 13.14.20", utils::Fmt(wformat, dt));
}