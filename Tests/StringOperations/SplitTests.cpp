#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"
//std
#include <sstream>

using utils::string_operations::Split;

TEST(SplitTests, PerfectCaseSkipEmptyTrue)
{
	const std::string commaSeparated{ "0,1,2,3,4,5,6,7,8,9" };

	const auto csResult = Split(commaSeparated, ',', true /*skipEmpty*/);
	EXPECT_EQ(10, csResult.size());
	EXPECT_EQ("0", csResult.front());
	EXPECT_EQ("5", csResult[5]);
	EXPECT_EQ("9", csResult.back());

	const std::string nlSeparated{ "0\n1\n2\n3\n4\n5\n6\n7\n8\n9" };
	
	const auto nlsResult = Split(nlSeparated, '\n', true /*skipEmpty*/);
	EXPECT_EQ(10, nlsResult.size());
	EXPECT_EQ("0", nlsResult.front());
	EXPECT_EQ("5", nlsResult[5]);
	EXPECT_EQ("9", nlsResult.back());
}

TEST(SplitTests, PerfectCaseSkipEmptyFalse)
{
	const std::string commaSeparated{ "0,1,2,3,4,5,6,7,8,9" };

	const auto csResult = Split(commaSeparated, ',', false /*skipEmpty*/);
	EXPECT_EQ(10, csResult.size());
	EXPECT_EQ("0", csResult.front());
	EXPECT_EQ("5", csResult[5]);
	EXPECT_EQ("9", csResult.back());

	const std::string nlSeparated{ "0\n1\n2\n3\n4\n5\n6\n7\n8\n9" };

	const auto nlsResult = Split(nlSeparated, '\n', false /*skipEmpty*/);
	EXPECT_EQ(10, nlsResult.size());
	EXPECT_EQ("0", nlsResult.front());
	EXPECT_EQ("5", nlsResult[5]);
	EXPECT_EQ("9", nlsResult.back());
}

TEST(SplitTests, SplitOnNonExistingCharSkipEmptyTrue)
{
	const std::string nlSeparated{ "0\n1\n2\n3\n4\n5\n6\n7\n8\n9" };

	{
		const auto nlsResult = Split(nlSeparated, 'n', true /*skipEmpty*/);
		EXPECT_EQ(1, nlsResult.size());
		EXPECT_EQ("0\n1\n2\n3\n4\n5\n6\n7\n8\n9", nlsResult.front());
	}

	{
		const auto nlsResult = Split(nlSeparated, '\\', true /*skipEmpty*/);
		EXPECT_EQ(1, nlsResult.size());
		EXPECT_EQ("0\n1\n2\n3\n4\n5\n6\n7\n8\n9", nlsResult.front());
	}
}

TEST(SplitTests, SplitOnNonExistingCharSkipEmptyFalse)
{
	const std::string nlSeparated{ "0\n1\n2\n3\n4\n5\n6\n7\n8\n9" };

	{
		const auto nlsResult = Split(nlSeparated, 'n', false /*skipEmpty*/);
		EXPECT_EQ(1, nlsResult.size());
		EXPECT_EQ("0\n1\n2\n3\n4\n5\n6\n7\n8\n9", nlsResult.front());
	}

	{
		const auto nlsResult = Split(nlSeparated, '\\', false /*skipEmpty*/);
		EXPECT_EQ(1, nlsResult.size());
		EXPECT_EQ("0\n1\n2\n3\n4\n5\n6\n7\n8\n9", nlsResult.front());
	}
}

TEST(SplitTests, MissingOneInTheMiddleSkipEmptyTrue)
{
	const std::string commaSeparated{ "0,1,2,3,4,,6,7,8,9" };

	const auto csResult = Split(commaSeparated, ',', true /*skipEmpty*/);
	EXPECT_EQ(9, csResult.size());
	EXPECT_EQ("0", csResult.front());
	EXPECT_EQ("6", csResult[5]);
	EXPECT_EQ("9", csResult.back());

	const std::string nlSeparated{ "0\n1\n2\n3\n4\n\n6\n7\n8\n9" };

	const auto nlsResult = Split(nlSeparated, '\n', true /*skipEmpty*/);
	EXPECT_EQ(9, nlsResult.size());
	EXPECT_EQ("0", nlsResult.front());
	EXPECT_EQ("6", nlsResult[5]);
	EXPECT_EQ("9", nlsResult.back());
}

TEST(SplitTests, MissingOneInTheMiddleSkipEmptyFalse)
{
	const std::string commaSeparated{ "0,1,2,3,4,,6,7,8,9" };

	const auto csResult = Split(commaSeparated, ',', false /*skipEmpty*/);
	EXPECT_EQ(10, csResult.size());
	EXPECT_EQ("0", csResult.front());
	EXPECT_EQ("", csResult[5]);
	EXPECT_EQ("9", csResult.back());

	const std::string nlSeparated{ "0\n1\n2\n3\n4\n\n6\n7\n8\n9" };

	const auto nlsResult = Split(nlSeparated, '\n', false /*skipEmpty*/);
	EXPECT_EQ(10, nlsResult.size());
	EXPECT_EQ("0", nlsResult.front());
	EXPECT_EQ("", nlsResult[5]);
	EXPECT_EQ("9", nlsResult.back());
}

TEST(SplitTests, MissingTwoInTheMiddleSkipEmptyTrue)
{
	const std::string commaSeparated{ "0,1,2,3,,,6,7,8,9" };

	const auto csResult = Split(commaSeparated, ',', true /*skipEmpty*/);
	EXPECT_EQ(8, csResult.size());
	EXPECT_EQ("0", csResult.front());
	EXPECT_EQ("7", csResult[5]);
	EXPECT_EQ("9", csResult.back());

	const std::string nlSeparated{ "0\n1\n2\n3\n\n\n6\n7\n8\n9" };

	const auto nlsResult = Split(nlSeparated, '\n', true /*skipEmpty*/);
	EXPECT_EQ(8, nlsResult.size());
	EXPECT_EQ("0", nlsResult.front());
	EXPECT_EQ("7", nlsResult[5]);
	EXPECT_EQ("9", nlsResult.back());
}

TEST(SplitTests, MissingTwoInTheMiddleSkipEmptyFalse)
{
	const std::string commaSeparated{ "0,1,2,3,,,6,7,8,9" };

	const auto csResult = Split(commaSeparated, ',', false /*skipEmpty*/);
	EXPECT_EQ(10, csResult.size());
	EXPECT_EQ("0", csResult.front());
	EXPECT_EQ("", csResult[4]);
	EXPECT_EQ("", csResult[5]);
	EXPECT_EQ("9", csResult.back());

	const std::string nlSeparated{ "0\n1\n2\n3\n\n\n6\n7\n8\n9" };

	const auto nlsResult = Split(nlSeparated, '\n', false /*skipEmpty*/);
	EXPECT_EQ(10, nlsResult.size());
	EXPECT_EQ("0", nlsResult.front());
	EXPECT_EQ("", nlsResult[4]);
	EXPECT_EQ("", nlsResult[5]);
	EXPECT_EQ("9", nlsResult.back());
}

TEST(SplitTests, MissingFirstOneSkipEmptyTrue)
{
	const std::string commaSeparated{ ",1,2,3,,,6,7,8,9" };

	const auto csResult = Split(commaSeparated, ',', true /*skipEmpty*/);
	EXPECT_EQ(7, csResult.size());
	EXPECT_EQ("1", csResult.front());
	EXPECT_EQ("8", csResult[5]);
	EXPECT_EQ("9", csResult.back());

	const std::string nlSeparated{ "\n1\n2\n3\n\n\n6\n7\n8\n9" };

	const auto nlsResult = Split(nlSeparated, '\n', true /*skipEmpty*/);
	EXPECT_EQ(7, nlsResult.size());
	EXPECT_EQ("1", nlsResult.front());
	EXPECT_EQ("8", nlsResult[5]);
	EXPECT_EQ("9", nlsResult.back());
}

TEST(SplitTests, MissingFirstOneSkipEmptyFalse)
{
	const std::string commaSeparated{ ",1,2,3,,,6,7,8,9" };

	const auto csResult = Split(commaSeparated, ',', false /*skipEmpty*/);
	EXPECT_EQ(10, csResult.size());
	EXPECT_EQ("", csResult.front());
	EXPECT_EQ("", csResult[4]);
	EXPECT_EQ("", csResult[5]);
	EXPECT_EQ("9", csResult.back());

	const std::string nlSeparated{ "\n1\n2\n3\n\n\n6\n7\n8\n9" };

	const auto nlsResult = Split(nlSeparated, '\n', false /*skipEmpty*/);
	EXPECT_EQ(10, nlsResult.size());
	EXPECT_EQ("", nlsResult.front());
	EXPECT_EQ("", nlsResult[4]);
	EXPECT_EQ("", nlsResult[5]);
	EXPECT_EQ("9", nlsResult.back());
}

TEST(SplitTests, MissingLastOneSkipEmptyTrue)
{
	const std::string commaSeparated{ ",1,2,3,,,6,7,8," };

	const auto csResult = Split(commaSeparated, ',', true /*skipEmpty*/);
	EXPECT_EQ(6, csResult.size());
	EXPECT_EQ("1", csResult.front());
	EXPECT_EQ("8", csResult[5]);
	EXPECT_EQ("8", csResult.back());

	const std::string nlSeparated{ "\n1\n2\n3\n\n\n6\n7\n8\n" };

	const auto nlsResult = Split(nlSeparated, '\n', true /*skipEmpty*/);
	EXPECT_EQ(6, nlsResult.size());
	EXPECT_EQ("1", nlsResult.front());
	EXPECT_EQ("8", nlsResult[5]);
	EXPECT_EQ("8", nlsResult.back());
}

TEST(SplitTests, MissingLastOneSkipEmptyFalse)
{
	const std::string commaSeparated{ ",1,2,3,,,6,7,8," };

	const auto csResult = Split(commaSeparated, ',', false /*skipEmpty*/);
	EXPECT_EQ(10, csResult.size());
	EXPECT_EQ("", csResult.front());
	EXPECT_EQ("", csResult[4]);
	EXPECT_EQ("", csResult[5]);
	EXPECT_EQ("", csResult.back());

	const std::string nlSeparated{ "\n1\n2\n3\n\n\n6\n7\n8\n" };

	const auto nlsResult = Split(nlSeparated, '\n', false /*skipEmpty*/);
	EXPECT_EQ(10, nlsResult.size());
	EXPECT_EQ("", nlsResult.front());
	EXPECT_EQ("", nlsResult[4]);
	EXPECT_EQ("", nlsResult[5]);
	EXPECT_EQ("", nlsResult.back());
}

TEST(SplitTests, NewLineAtTheEndSkipEmptyTrue)
{
	const std::string commaSeparated{ ",1,2,3,,,6,7,8,\n" };

	const auto csResult = Split(commaSeparated, ',', true /*skipEmpty*/);
	EXPECT_EQ(7, csResult.size());
	EXPECT_EQ("1", csResult.front());
	EXPECT_EQ("7", csResult[4]);
	EXPECT_EQ("8", csResult[5]);
	EXPECT_EQ("\n", csResult.back());

	const std::string nlSeparated{ "\n1\n2\n3\n\n\n6\n7\n8\n\n" };

	const auto nlsResult = Split(nlSeparated, '\n', true /*skipEmpty*/);
	EXPECT_EQ(6, nlsResult.size());
	EXPECT_EQ("1", nlsResult.front());
	EXPECT_EQ("7", nlsResult[4]);
	EXPECT_EQ("8", nlsResult[5]);
	EXPECT_EQ("8", nlsResult.back());
}

TEST(SplitTests, NewLineAtTheEndSkipEmptyFalse)
{
	const std::string commaSeparated{ ",1,2,3,,,6,7,8,\n" };

	const auto csResult = Split(commaSeparated, ',', false /*skipEmpty*/);
	EXPECT_EQ(10, csResult.size());
	EXPECT_EQ("", csResult.front());
	EXPECT_EQ("", csResult[4]);
	EXPECT_EQ("", csResult[5]);
	EXPECT_EQ("\n", csResult.back());

	const std::string nlSeparated{ "\n1\n2\n3\n\n\n6\n7\n8\n\n" };

	const auto nlsResult = Split(nlSeparated, '\n', false /*skipEmpty*/);
	EXPECT_EQ(11, nlsResult.size());
	EXPECT_EQ("", nlsResult.front());
	EXPECT_EQ("", nlsResult[4]);
	EXPECT_EQ("", nlsResult[5]);
	EXPECT_EQ("", nlsResult.back());
}

TEST(SplitTests, AllEmptySkipEmptyTrue)
{
	const std::string commaSeparated{ ",,,,,,,,," };

	const auto csResult = Split(commaSeparated, ',', true /*skipEmpty*/);
	EXPECT_EQ(0, csResult.size());
	
	const std::string nlSeparated{ "\n\n\n\n\n\n\n\n\n\n" };

	const auto nlsResult = Split(nlSeparated, '\n', true /*skipEmpty*/);
	EXPECT_EQ(0, nlsResult.size());
}

TEST(SplitTests, AllEmptySkipEmptyFalse)
{
	const std::string commaSeparated{ ",,,,,,,,," };

	const auto csResult = Split(commaSeparated, ',', false /*skipEmpty*/);
	EXPECT_EQ(10, csResult.size());
	EXPECT_EQ("", csResult.front());
	EXPECT_EQ("", csResult[4]);
	EXPECT_EQ("", csResult[5]);
	EXPECT_EQ("", csResult.back());

	const std::string nlSeparated{ "\n\n\n\n\n\n\n\n\n" };

	const auto nlsResult = Split(nlSeparated, '\n', false /*skipEmpty*/);
	EXPECT_EQ(10, nlsResult.size());
	EXPECT_EQ("", nlsResult.front());
	EXPECT_EQ("", nlsResult[4]);
	EXPECT_EQ("", nlsResult[5]);
	EXPECT_EQ("", nlsResult.back());
}
