#include "Utils/Utils.h"

//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"

TEST(DeepCopyPointer, CopyCtor)
{
	utils::DeepCopyPointer dcp1(new int(0));
	EXPECT_EQ(*dcp1, 0);
	EXPECT_TRUE(dcp1);

	(*dcp1)++;
	EXPECT_EQ(*dcp1, 1);

	utils::DeepCopyPointer dcp2(dcp1);	//copy ctor, makes a deep copy
	EXPECT_TRUE(dcp2);
	EXPECT_EQ(*dcp2, 1);
	EXPECT_TRUE(dcp1 == dcp2);	//explicit operator== (deep comparison)
	EXPECT_FALSE(dcp1 != dcp2);	//explicit operator!= (deep comparison)
	EXPECT_EQ(dcp1, dcp2);		//implicit operator== (deep comparison)
	EXPECT_EQ(*dcp1, *dcp2);		//int ==
	EXPECT_NE(dcp1.Get(), dcp2.Get());		//addresses are not equal

	(*dcp2)++;
	EXPECT_EQ(*dcp2, 2);
	EXPECT_EQ(*dcp1, 1);
	EXPECT_TRUE(dcp1 != dcp2);	//explicit operator!= (deep comparison)
	EXPECT_FALSE(dcp1 == dcp2);	//explicit operator== (deep comparison)
	EXPECT_NE(dcp1, dcp2);		//implicit operator== (deep comparison)
	EXPECT_NE(*dcp1, *dcp2);		//int ==
	EXPECT_NE(dcp1.Get(), dcp2.Get());		//addresses are not equal
}

TEST(DeepCopyPointer, CopyAssignment)
{
	utils::DeepCopyPointer dcp1(new int(0));
	EXPECT_EQ(*dcp1, 0);
	EXPECT_TRUE(dcp1);

	(*dcp1)++;
	EXPECT_EQ(*dcp1, 1);

	utils::DeepCopyPointer<int> dcp2;	//default ctor
	EXPECT_FALSE(dcp2);

	dcp2 = dcp1;	//copy assignment, makes a deep copy
	EXPECT_EQ(*dcp2, 1);
	EXPECT_TRUE(dcp1 == dcp2);	//explicit operator== (deep comparison)
	EXPECT_FALSE(dcp1 != dcp2);	//explicit operator!= (deep comparison)
	EXPECT_EQ(dcp1, dcp2);		//implicit operator== (deep comparison)
	EXPECT_EQ(*dcp1, *dcp2);		//int ==
	EXPECT_NE(dcp1.Get(), dcp2.Get());		//addresses are not equal

	(*dcp2)++;
	EXPECT_EQ(*dcp2, 2);
	EXPECT_EQ(*dcp1, 1);
	EXPECT_TRUE(dcp1 != dcp2);	//explicit operator!= (deep comparison)
	EXPECT_FALSE(dcp1 == dcp2);	//explicit operator== (deep comparison)
	EXPECT_NE(dcp1, dcp2);		//implicit operator== (deep comparison)
	EXPECT_NE(*dcp1, *dcp2);		//int ==
	EXPECT_NE(dcp1.Get(), dcp2.Get());		//addresses are not equal
}

struct AIntWrapper
{
	static AIntWrapper* MakeCloneRawPtr(const AIntWrapper& iiw) { return  iiw.CloneRawPtr(); }


	virtual AIntWrapper* CloneRawPtr() const = 0;
	virtual int GetValue() const = 0;
};

struct IntWrapper : public AIntWrapper
{
	int value;

	IntWrapper(int value) : value(value) {}

	virtual int GetValue() const override { return value; }

	// Inherited via IIntWrapper
	virtual IntWrapper* CloneRawPtr() const override
	{
		return new IntWrapper{ 100 };	//stupid clone function. Always returns 100 for easy testability
	}
};

TEST(DeepCopyPointer, ArrowOperator)
{
	utils::DeepCopyPointer dcp1(new IntWrapper{ 0 });
	EXPECT_TRUE(dcp1);
	EXPECT_EQ(dcp1->value, 0);
	EXPECT_EQ(dcp1->GetValue(), 0);

	dcp1->value++;
	EXPECT_EQ(dcp1->value, 1);
	EXPECT_EQ(dcp1->GetValue(), 1);

	utils::DeepCopyPointer<IntWrapper> dcp2;	//default ctor
	EXPECT_FALSE(dcp2);

	dcp2 = dcp1;	//copy assignment, makes a deep copy
	EXPECT_EQ(dcp2->value, 1);
	EXPECT_EQ(dcp2->GetValue(), 1);
	EXPECT_EQ(dcp1->value, 1);
	EXPECT_EQ(dcp1->GetValue(), 1);
	EXPECT_NE(dcp1.Get(), dcp2.Get());		//addresses are not equal

	dcp2->value++;
	EXPECT_EQ(dcp2->value, 2);
	EXPECT_EQ(dcp2->GetValue(), 2);
	EXPECT_EQ(dcp1->value, 1);
	EXPECT_EQ(dcp1->GetValue(), 1);
	EXPECT_NE(dcp1.Get(), dcp2.Get());		//addresses are not equal
}

TEST(DeepCopyPointer, CloneFunc)
{
	utils::DeepCopyPointer<AIntWrapper> dcp1(new IntWrapper(0), AIntWrapper::MakeCloneRawPtr);
	EXPECT_TRUE(dcp1);
	EXPECT_EQ(dcp1->GetValue(), 0);

	utils::DeepCopyPointer dcp2(dcp1);
	utils::DeepCopyPointer dcp3 = dcp1;
	EXPECT_EQ(dcp2->GetValue(), 100);
	EXPECT_EQ(dcp3->GetValue(), 100);
}

TEST(DeepCopyPointer, Reset)
{
	int* pi1 = new int(1);
	int* pi2 = new int(2);

	utils::DeepCopyPointer dcp1(pi1);
	EXPECT_EQ(*dcp1, 1);

	utils::DeepCopyPointer<int> dcp2;
	dcp2.Reset(pi2);
	EXPECT_EQ(*dcp2, 2);
}

int* GetNewInt(const int&)
{
	return new int(11);
}

TEST(DeepCopyPointer, MoveSemantics)
{
	int* pi1 = new int(1);
	utils::DeepCopyPointer dcp1(pi1, GetNewInt);
	EXPECT_EQ(*dcp1, 1);

	utils::DeepCopyPointer dcp2(std::move(dcp1));
	EXPECT_EQ(*dcp2, 1);

	utils::DeepCopyPointer<int> dcp3;
	EXPECT_FALSE(dcp3);

	dcp3 = std::move(dcp2);
	EXPECT_TRUE(dcp3);
	EXPECT_EQ(*dcp3, 1);
}

TEST(DeepCopyPointer, CopyFromEmpty)
{
	const utils::DeepCopyPointer<int> dcp1;
	utils::DeepCopyPointer dcp2(dcp1);
	utils::DeepCopyPointer<int> dcp3;
	dcp3 = dcp1;

	utils::DeepCopyPointer<int> dcp4(new int(4));
	EXPECT_NE(dcp1, dcp4);
	dcp4 = dcp1;
	EXPECT_EQ(dcp1, dcp4);
}
