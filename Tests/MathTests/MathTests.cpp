#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"

TEST(Math, FloorToNearestMultipleOf)
{
	//step 0.5
	EXPECT_DOUBLE_EQ(0.0, utils::math::FloorToNearestMultipleOf(0.14, 0.5));
	EXPECT_DOUBLE_EQ(0.0, utils::math::FloorToNearestMultipleOf(0.24, 0.5));

	EXPECT_DOUBLE_EQ(0.5, utils::math::FloorToNearestMultipleOf(0.52, 0.5));
	EXPECT_DOUBLE_EQ(0.5, utils::math::FloorToNearestMultipleOf(0.58, 0.5));
	EXPECT_DOUBLE_EQ(0.5, utils::math::FloorToNearestMultipleOf(0.85, 0.5));
	EXPECT_DOUBLE_EQ(0.5, utils::math::FloorToNearestMultipleOf(0.94, 0.5));

	EXPECT_DOUBLE_EQ(1.0, utils::math::FloorToNearestMultipleOf(1.25, 0.5));
	EXPECT_DOUBLE_EQ(1.0, utils::math::FloorToNearestMultipleOf(1.49, 0.5));

	//negatives
	EXPECT_DOUBLE_EQ(-0.5, utils::math::FloorToNearestMultipleOf(-0.25, 0.5));
	EXPECT_DOUBLE_EQ(-0.5, utils::math::FloorToNearestMultipleOf(-0.39, 0.5));
	EXPECT_DOUBLE_EQ(-1.0, utils::math::FloorToNearestMultipleOf(-0.65, 0.5));
	EXPECT_DOUBLE_EQ(-1.0, utils::math::FloorToNearestMultipleOf(-0.74, 0.5));
}

TEST(Math, CeilToNearestMultipleOf)
{
	//step 0.5
	EXPECT_DOUBLE_EQ(0.5, utils::math::CeilToNearestMultipleOf(0.01, 0.5));
	EXPECT_DOUBLE_EQ(0.5, utils::math::CeilToNearestMultipleOf(0.28, 0.5));
	EXPECT_DOUBLE_EQ(0.5, utils::math::CeilToNearestMultipleOf(0.48, 0.5));

	EXPECT_DOUBLE_EQ(1.0, utils::math::CeilToNearestMultipleOf(0.501, 0.5));
	EXPECT_DOUBLE_EQ(1.0, utils::math::CeilToNearestMultipleOf(0.99, 0.5));

	//negatives
	EXPECT_DOUBLE_EQ(-0.5, utils::math::CeilToNearestMultipleOf(-0.55, 0.5));
	EXPECT_DOUBLE_EQ(-0.5, utils::math::CeilToNearestMultipleOf(-0.89, 0.5));
	EXPECT_DOUBLE_EQ(-1.0, utils::math::CeilToNearestMultipleOf(-1.15, 0.5));
	EXPECT_DOUBLE_EQ(-1.0, utils::math::CeilToNearestMultipleOf(-1.48, 0.5));
}


TEST(Math, RoundToNearestMultipleOf)
{
	//step 0.5
	EXPECT_DOUBLE_EQ(0.0, utils::math::RoundToNearestMultipleOf(0.14, 0.5));
	EXPECT_DOUBLE_EQ(0.0, utils::math::RoundToNearestMultipleOf(0.24, 0.5));
	
	EXPECT_DOUBLE_EQ(0.5, utils::math::RoundToNearestMultipleOf(0.25, 0.5));
	EXPECT_DOUBLE_EQ(0.5, utils::math::RoundToNearestMultipleOf(0.38, 0.5));	
	EXPECT_DOUBLE_EQ(0.5, utils::math::RoundToNearestMultipleOf(0.65, 0.5));	
	EXPECT_DOUBLE_EQ(0.5, utils::math::RoundToNearestMultipleOf(0.74, 0.5));	
	
	EXPECT_DOUBLE_EQ(1.0, utils::math::RoundToNearestMultipleOf(0.75, 0.5));
	EXPECT_DOUBLE_EQ(1.0, utils::math::RoundToNearestMultipleOf(0.88, 0.5));

	//step 0.8
	EXPECT_DOUBLE_EQ(0.0, utils::math::RoundToNearestMultipleOf(0.14, 0.8));
	EXPECT_DOUBLE_EQ(0.0, utils::math::RoundToNearestMultipleOf(0.39, 0.8));

	EXPECT_DOUBLE_EQ(1.6, utils::math::RoundToNearestMultipleOf(1.2, 0.8));
	EXPECT_DOUBLE_EQ(1.6, utils::math::RoundToNearestMultipleOf(1.44, 0.8));
	EXPECT_DOUBLE_EQ(1.6, utils::math::RoundToNearestMultipleOf(1.86, 0.8));
	EXPECT_DOUBLE_EQ(1.6, utils::math::RoundToNearestMultipleOf(1.99, 0.8));

	//negatives
	EXPECT_DOUBLE_EQ(0.0, utils::math::RoundToNearestMultipleOf(-0.14, 0.5));
	EXPECT_DOUBLE_EQ(0.0, utils::math::RoundToNearestMultipleOf(-0.24, 0.5));

	EXPECT_DOUBLE_EQ(-0.5, utils::math::RoundToNearestMultipleOf(-0.25, 0.5));
	EXPECT_DOUBLE_EQ(-0.5, utils::math::RoundToNearestMultipleOf(-0.39, 0.5));
	EXPECT_DOUBLE_EQ(-0.5, utils::math::RoundToNearestMultipleOf(-0.65, 0.5));
	EXPECT_DOUBLE_EQ(-0.5, utils::math::RoundToNearestMultipleOf(-0.74, 0.5));
}

TEST(Math, Scale)
{
	//scale extreme points
	EXPECT_DOUBLE_EQ(0.0, utils::Scale(1853, 1877, 1853, 0, 100));
	EXPECT_DOUBLE_EQ(100.0, utils::Scale(1853, 1877, 1877, 0, 100));

	//scale extreme points, invert range
	EXPECT_DOUBLE_EQ(100.0, utils::Scale(1853, 1877, 1853, 100, 0));
	EXPECT_DOUBLE_EQ(0.0, utils::Scale(1853, 1877, 1877, 100, 0));

	//scale middle points
	EXPECT_DOUBLE_EQ(60.0, utils::Scale(5, 15, 10, 10, 110));

	//scale points outside of range
	EXPECT_DOUBLE_EQ(-30.0, utils::Scale(5, 15, 1, 10, 110));
	EXPECT_DOUBLE_EQ(130.0, utils::Scale(5, 15, 17, 10, 110));

	//zero ranges
	EXPECT_THROW(utils::Scale(5, 5, 3, 10, 110), std::invalid_argument);
	EXPECT_DOUBLE_EQ(10.0, utils::Scale(5, 15, 12, 10, 10));

}