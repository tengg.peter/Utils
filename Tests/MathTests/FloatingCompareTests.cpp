#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"

using namespace utils;

TEST(Math, FloatEquals_float_defaultEpsilon)
{
	EXPECT_TRUE(utils::Equals(0.2f * FLOAT_EPSILON, -0.2f * FLOAT_EPSILON));
	EXPECT_FALSE(utils::Equals(-FLOAT_EPSILON, FLOAT_EPSILON));
}

TEST(Math, FloatEquals_float)
{
	constexpr float epsilon = 0.001f;

	EXPECT_TRUE(utils::Equals(0.2f * epsilon, -0.2f * epsilon, epsilon));
	EXPECT_FALSE(utils::Equals(-epsilon, epsilon, epsilon));
}

TEST(Math, FloatEquals_double_defaultEpsilon)
{
	EXPECT_TRUE(utils::Equals(0.2 * DOUBLE_EPSILON, -0.2 * DOUBLE_EPSILON));
	EXPECT_FALSE(utils::Equals(-DOUBLE_EPSILON, DOUBLE_EPSILON));
}

TEST(Math, FloatEquals_double)
{
	constexpr double epsilon = 0.001;

	EXPECT_TRUE(utils::Equals(0.2 * epsilon, -0.2 * epsilon, epsilon));
	EXPECT_FALSE(utils::Equals(-epsilon, epsilon, epsilon));
}
