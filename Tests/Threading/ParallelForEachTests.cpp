#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"
//std
#include <numeric>
#include <thread>

using std::endl;

using namespace std::chrono_literals;

namespace
{
	constexpr bool SKIP_THREADING_TESTS = false;	//because they take a long time

	auto doubleItems = [](int& i)
	{
		std::this_thread::sleep_for(500ms);
		i *= 2;
	};

	void PrintThreadNumbers(const std::vector<int>& threadNumbers)
	{
		for (const int i : threadNumbers)
		{
			TCOUT << i << ", ";
		}
	}

	auto writeThreadNumber = [](int& i)
	{
		std::this_thread::sleep_for(500ms);
		i = utils::threading::MyThreadNumber();
	};

	int MaxThreads()
	{
		int hwc = static_cast<int>(std::thread::hardware_concurrency());
		return std::max(hwc - 2, 1);
	}
}


TEST(Threading, ParallelForEach_0_item)
{
	if constexpr (SKIP_THREADING_TESTS)
	{
		GTEST_SKIP();	//skip, because takes a long time
	}
	std::vector<int> ints;
	utils::ParallelForEach(ints, doubleItems);

	EXPECT_TRUE(ints.empty());
}

TEST(Threading, ParallelForEach_1_item)
{
	if constexpr (SKIP_THREADING_TESTS)
	{
		GTEST_SKIP();	//skip, because takes a long time
	}
	std::vector<int> ints{ 3 };
	utils::ParallelForEach(ints, doubleItems);

	EXPECT_EQ(1, ints.size());
	EXPECT_EQ(6, ints.front());
}

TEST(Threading, ParallelForEach_50_items)
{
	if constexpr (SKIP_THREADING_TESTS)
	{
		GTEST_SKIP();	//skip, because takes a long time
	}
	std::vector<int> ints(50);
	std::iota(ints.begin(), ints.end(), 0);

	utils::ParallelForEach(ints, doubleItems, 10);

	for (size_t i = 0; i < ints.size(); i++)
	{
		EXPECT_EQ(i * 2, ints[i]);
	}
}

TEST(Threading, ParallelForEach_numThreadsHint_defaultValue)
{
	if constexpr (SKIP_THREADING_TESTS)
	{
		GTEST_SKIP();	//skip, because takes a long time
	}
	std::vector<int> threadNumbers(50);

	utils::ParallelForEach(threadNumbers, writeThreadNumber);	//default value is 0

	EXPECT_EQ(0, *std::min_element(threadNumbers.cbegin(), threadNumbers.cend()));
	EXPECT_EQ(MaxThreads() - 1, *(std::max_element(threadNumbers.cbegin(), threadNumbers.cend())));
}

TEST(Threading, ParallelForEach_numThreadsHint_0)
{
	if constexpr (SKIP_THREADING_TESTS)
	{
		GTEST_SKIP();	//skip, because takes a long time
	}
	std::vector<int> threadNumbers(50);

	utils::ParallelForEach(threadNumbers, writeThreadNumber, 0 /*numThreadsHint*/);

	EXPECT_EQ(0, *std::min_element(threadNumbers.cbegin(), threadNumbers.cend()));
	EXPECT_EQ(MaxThreads() - 1, *(std::max_element(threadNumbers.cbegin(), threadNumbers.cend())));
}

TEST(Threading, ParallelForEach_numThreadsHint_1)
{
	if constexpr (SKIP_THREADING_TESTS)
	{
		GTEST_SKIP();	//skip, because takes a long time
	}
	std::vector<int> threadNumbers(5);

	utils::ParallelForEach(threadNumbers, writeThreadNumber, 1 /*numThreadsHint*/);

	EXPECT_TRUE(std::equal(threadNumbers.cbegin() + 1, threadNumbers.cend(), threadNumbers.cbegin()));
}

TEST(Threading, ParallelForEach_numThreadsHint_generalCase_8)
{
	if constexpr (SKIP_THREADING_TESTS)
	{
		GTEST_SKIP();	//skip, because takes a long time
	}
	std::vector<int> threadNumbers(50);

	utils::ParallelForEach(threadNumbers, writeThreadNumber, 8 /*numThreadsHint*/);

	std::set<int> s(threadNumbers.cbegin(), threadNumbers.cend());
	EXPECT_EQ(8, s.size());
}

TEST(Threading, ParallelForEach_numThreadsHint_14_maxThreadsThatTheLogicAllows)
{
	if constexpr (SKIP_THREADING_TESTS)
	{
		GTEST_SKIP();	//skip, because takes a long time
	}
	std::vector<int> threadNumbers(50);

	utils::ParallelForEach(threadNumbers, writeThreadNumber, 14 /*numThreadsHint*/);

	EXPECT_EQ(0, *std::min_element(threadNumbers.cbegin(), threadNumbers.cend()));
  //13 is the magic number here, because it will run 14 threads (which is the max for my Asus laptop). Thus the greatest thread number will be 13
	EXPECT_EQ(13, *(std::max_element(threadNumbers.cbegin(), threadNumbers.cend())));
}

TEST(Threading, ParallelForEach_numThreadsHint_16_equalsHwConcurrency)
{
	if constexpr (SKIP_THREADING_TESTS)
	{
		GTEST_SKIP();	//skip, because takes a long time
	}
	std::vector<int> threadNumbers(50);

	utils::ParallelForEach(threadNumbers, writeThreadNumber, 16 /*numThreadsHint*/);

	EXPECT_EQ(0, *std::min_element(threadNumbers.cbegin(), threadNumbers.cend()));
	EXPECT_EQ(13, *(std::max_element(threadNumbers.cbegin(), threadNumbers.cend())));
}

TEST(Threading, ParallelForEach_numThreadsHint_32_greaterThanHwConcurrency)
{
	if constexpr (SKIP_THREADING_TESTS)
	{
		GTEST_SKIP();	//skip, because takes a long time
	}
	std::vector<int> threadNumbers(50);

	utils::ParallelForEach(threadNumbers, writeThreadNumber, 32 /*numThreadsHint*/);

	EXPECT_EQ(0, *std::min_element(threadNumbers.cbegin(), threadNumbers.cend()));
	EXPECT_EQ(13, *(std::max_element(threadNumbers.cbegin(), threadNumbers.cend())));
}

TEST(Threading, ParallelForEach_abortTrueInitially)
{
	if constexpr (SKIP_THREADING_TESTS)
	{
		GTEST_SKIP();	//skip, because takes a long time
	}
	std::vector<int> threadNumbers(50);

	std::atomic_bool abort = true;
	utils::ParallelForEach(threadNumbers, writeThreadNumber, 0, &abort);

	EXPECT_EQ(0, *std::min_element(threadNumbers.cbegin(), threadNumbers.cend()));
	EXPECT_EQ(0, *(std::max_element(threadNumbers.cbegin(), threadNumbers.cend())));
}

TEST(Threading, ParallelForEach_abort)
{
	if constexpr (SKIP_THREADING_TESTS)
	{
		GTEST_SKIP();	//skip, because takes a long time
	}
	std::vector<int> threadNumbers(50, -1);

	std::atomic_bool abort = false;
	std::thread t([&abort]()
		{
			std::this_thread::sleep_for(200ms);
			abort = true;
		});
	utils::ParallelForEach(threadNumbers, writeThreadNumber, 0, &abort);
	t.join();

	//does not have time to start all threads and process all items because of the abort call
	EXPECT_LT(threadNumbers.size() / 2, std::count(threadNumbers.cbegin(), threadNumbers.cend(), -1));	//more than half of the items should remain unprocessed
}