#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"
//std
#include <thread>
#include <vector>


TEST(Threading, MyThreadNumber)
{
	constexpr int numThreads = 10;
	std::vector<std::thread> threads;
	std::vector<std::vector<int>> queriedThreadNumbers(numThreads);

	for (size_t i = 0; i < numThreads; i++)
	{
		threads.push_back(std::thread(
			[i, &queriedThreadNumbers]()
		{
			int reps = 10;
			while (reps--)
			{
				queriedThreadNumbers[i].push_back(utils::threading::MyThreadNumber());
			}
		}));
	}

	for (auto& th : threads)
	{
		th.join();
	}

	for (const std::vector<int> numbers : queriedThreadNumbers)
	{
		for (const int i: numbers)
		{
			EXPECT_EQ(numbers.front(), i);
		}
	}
}

TEST(Threading, AcquireNewThreadNumber)
{
	constexpr int numThreads = 10;
	std::vector<std::thread> threads;
	std::vector<std::vector<int>> queriedThreadNumbers(numThreads);

	for (size_t i = 0; i < numThreads; i++)
	{
		threads.push_back(std::thread(
			[i, &queriedThreadNumbers]()
		{
			int reps = 10;
			while (reps--)
			{
				queriedThreadNumbers[i].push_back(utils::threading::AcquireNewThreadNumber());
			}
		}));
	}

	for (auto& th : threads)
	{
		th.join();
	}

	for (const std::vector<int> numbers : queriedThreadNumbers)
	{
		for (size_t i = 0; i < numbers.size() - 1; i++)
		{
			EXPECT_LT(numbers[i], numbers[i + 1]);
		}
	}
}
