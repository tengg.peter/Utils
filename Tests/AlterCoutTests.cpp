#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"
//std
#include <sstream>

using std::endl;
using utils::MuteCout;
using utils::RedirectCout;


TEST(MuteCout, Test)
{
	utils::StringStream buffer;
	RedirectCout redirect(buffer.rdbuf());

	TCOUT << "bla" << endl;
	EXPECT_EQ(4, buffer.str().size());
	buffer.str(STR(""));

	{
		MuteCout mute(true);
		TCOUT << "bla" << endl;
		EXPECT_TRUE(buffer.str().empty());
	}

	TCOUT << "bla" << endl;
	EXPECT_EQ(4, buffer.str().size());
	buffer.str(STR(""));

	{
		MuteCout mute(false);
		TCOUT << "bla" << endl;
		EXPECT_EQ(4, buffer.str().size());
		buffer.str(STR(""));
	}

	TCOUT << "bla" << endl;
	EXPECT_EQ(4, buffer.str().size());
	buffer.str(STR(""));
}