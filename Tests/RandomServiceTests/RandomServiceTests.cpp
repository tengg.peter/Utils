#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"
//std

TEST(RandomService, GetSample)
{
	auto& rs = utils::RandomService::GetInstance();

	EXPECT_EQ(std::vector<size_t>{11}, rs.GetSample(11, 11, 1));
	EXPECT_EQ(std::vector<size_t>{}, rs.GetSample(11, 11, 0));
	EXPECT_THROW(rs.GetSample(11, 10, 1), std::invalid_argument);
	EXPECT_EQ(std::vector<size_t>({ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }), rs.GetSample(1, 11, 20));

	for (size_t i = 0; i < 1000; i++)
	{
		const auto sample = rs.GetSample(1, 11, 4);
		EXPECT_EQ(4, sample.size());
		for (const size_t n : sample)
		{
			EXPECT_LE(1, n);
			EXPECT_GE(11, n);
		}
	}
}

TEST(RandomService, GetUniformTemplate)
{
	auto& rs = utils::RandomService::GetInstance();

	const double d = rs.GetUniform(1.0, 9.0);
	EXPECT_LE(1.0, d);
	EXPECT_GE(9.0, d);

	const float f = rs.GetUniform(2.0f, 5.0f);
	EXPECT_LE(2.0f, f);
	EXPECT_GE(5.0f, f);

	const int i = rs.GetUniform(0, 50);
	EXPECT_LE(0, i);
	EXPECT_GE(50, i);

	const bool b = rs.GetUniform(false, true);
	EXPECT_TRUE(true == b || false == b);
}