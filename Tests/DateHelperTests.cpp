#include "Utils/Utils.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include "gtest/gtest.h"

using namespace utils::date_and_time;

TEST(DateHelperTests, SecsToTime)
{
	const utils::String timeString1{ STR("01:02:03") };
	const utils::String timeString2 = utils::date_and_time::SecsToTime(1 * 3600 + 2 * 60 + 3);

	EXPECT_EQ(timeString1, timeString2);
}

TEST(DateHelperTests, MonthToString)
{
	EXPECT_EQ(STR("Jan"), utils::MonthToString(1));
	EXPECT_EQ(STR("Jan"), utils::MonthToString(DateTime(2022, 1, 1, 0, 0, 0)));
	EXPECT_EQ(STR("Feb"), utils::MonthToString(2));
	EXPECT_EQ(STR("Feb"), utils::MonthToString(DateTime(2022, 2, 1, 0, 0, 0)));
	EXPECT_EQ(STR("Mar"), utils::MonthToString(3));
	EXPECT_EQ(STR("Mar"), utils::MonthToString(DateTime(2022, 3, 1, 0, 0, 0)));
	EXPECT_EQ(STR("Apr"), utils::MonthToString(4));
	EXPECT_EQ(STR("Apr"), utils::MonthToString(DateTime(2022, 4, 1, 0, 0, 0)));
	EXPECT_EQ(STR("May"), utils::MonthToString(5));
	EXPECT_EQ(STR("May"), utils::MonthToString(DateTime(2022, 5, 1, 0, 0, 0)));
	EXPECT_EQ(STR("Jun"), utils::MonthToString(6));
	EXPECT_EQ(STR("Jun"), utils::MonthToString(DateTime(2022, 6, 1, 0, 0, 0)));
	EXPECT_EQ(STR("Jul"), utils::MonthToString(7));
	EXPECT_EQ(STR("Jul"), utils::MonthToString(DateTime(2022, 7, 1, 0, 0, 0)));
	EXPECT_EQ(STR("Aug"), utils::MonthToString(8));
	EXPECT_EQ(STR("Aug"), utils::MonthToString(DateTime(2022, 8, 1, 0, 0, 0)));
	EXPECT_EQ(STR("Sep"), utils::MonthToString(9));
	EXPECT_EQ(STR("Sep"), utils::MonthToString(DateTime(2022, 9, 1, 0, 0, 0)));
	EXPECT_EQ(STR("Oct"), utils::MonthToString(10));
	EXPECT_EQ(STR("Oct"), utils::MonthToString(DateTime(2022, 10, 1, 0, 0, 0)));
	EXPECT_EQ(STR("Nov"), utils::MonthToString(11));
	EXPECT_EQ(STR("Nov"), utils::MonthToString(DateTime(2022, 11, 1, 0, 0, 0)));
	EXPECT_EQ(STR("Dec"), utils::MonthToString(12));
	EXPECT_EQ(STR("Dec"), utils::MonthToString(DateTime(2022, 12, 1, 0, 0, 0)));
}