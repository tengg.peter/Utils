#pragma once
#include "FloatingCompare.h"
//std
#include <cmath>
#include <exception>

namespace utils::math
{
	template<typename T>
	inline double FloorToNearestMultipleOf(T x, T step)
	{
		step = std::abs(step);
		return floor(x / step) * step;
	}

	template<typename T>
	inline double CeilToNearestMultipleOf(T x, T step)
	{
		step = std::abs(step);
		return ceil(x / step) * step;
	}

	template<typename T>
	inline double RoundToNearestMultipleOf(T x, T step)
	{
		if (x >= 0)
		{
			return FloorToNearestMultipleOf(x + step / 2, step);
		}
		return CeilToNearestMultipleOf(x - step / 2, step);
	}

	template <typename T>
	int Sgn(T val)
	{
		return (T(0) < val) - (val < T(0));
	}

}

namespace utils
{
	inline double Scale(double fromRangeLow, double fromRangeHigh, double x, double toRangeLow, double toRangeHigh)
	{
		const double fromRange = fromRangeHigh - fromRangeLow;
		const double toRange = toRangeHigh - toRangeLow;
		const double scale = toRange / fromRange;

		if (std::isinf(scale))
		{
			throw std::invalid_argument("Source range size cannot be zero.");
		}
		return scale * (x - fromRangeLow) + toRangeLow;
	}
}