#pragma once

#include <cmath>

namespace utils
{
	constexpr double DOUBLE_EPSILON = 0.000001;
	constexpr float FLOAT_EPSILON = 0.000001f;

	template<typename TFloat>
	TFloat DefaultEpsilon()
	{
		if constexpr (std::is_same<double, TFloat>::value)
		{
			return DOUBLE_EPSILON;
		}
		else if constexpr (std::is_same<float, TFloat>::value)
		{
			return FLOAT_EPSILON;
		}
		else
		{
			[]<bool flag = false>()
            	{ static_assert(flag, "Type not supported."); }();
		}
	}

	template<typename TFloat>
	inline bool Equals(TFloat a, TFloat b, TFloat epsilon = DefaultEpsilon<TFloat>())
	{
		return std::abs(a - b) < epsilon;
	}
}