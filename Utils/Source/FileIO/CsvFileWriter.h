#pragma once
#include "Utils/Source/Defines.h"
#include "Utils/Source/StringOperations/StringConversion.h"

#include <codecvt>
#include <locale>
#include <map>
#include <vector>

namespace utils::file_io
{
	class CsvFileWriter
	{
	public:
		explicit CsvFileWriter(const String& fileName, bool append, char separator = ';')
		    : m_fileName(fileName), m_separator(separator)
		{
			const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
			m_file.imbue(utf8_locale);
			m_file.open(fileName, append ? OfStream::app : OfStream::trunc);
		}

		//rule of three
		~CsvFileWriter() { m_file.close(); }
		CsvFileWriter(const CsvFileWriter&) = delete;
		CsvFileWriter& operator=(const CsvFileWriter&) = delete;

		void AddTable(size_t row, size_t col, const std::vector<std::vector<String>>& table);
		void ClearAllTables();
		void PrintTables();
		void Flush() { m_file.flush(); }

		//writes an empty cell by insering a semi colon
		void Write()
		{
			ThrowIfNotOk();
			m_file << m_separator;
		}

		template <typename T>
		void Write(T last)
		{
			ThrowIfNotOk();
			m_file << last << m_separator;
		}

		template <typename T, typename... Args>
		void Write(T first, Args... args)
		{
			ThrowIfNotOk();
			m_file << first << m_separator;
			Write(args...);
		}

		//writes a line break
		void WriteLine()
		{
			ThrowIfNotOk();
			m_file << '\n';
		}

		template <typename T>
		void WriteLine(T last)
		{
			ThrowIfNotOk();
			m_file << last << '\n';
		}

		template <typename T, typename... Args>
		void WriteLine(T first, Args... args)
		{
			ThrowIfNotOk();
			m_file << first << m_separator;
			WriteLine(args...);
		}

	private:
		void ThrowIfNotOk()
		{
			if (m_file.fail())
			{
#ifdef _WIN32
				throw std::exception(
				    string_conversion::UtilsStringToStdString(
				        L"Could not open file: " + m_fileName)
				        .c_str());
#else
				throw std::runtime_error(
				    ("Could not open file: " + m_fileName).c_str());
#endif
			}
		}
		
		const String m_fileName;
		const char m_separator;

		OfStream m_file;
		std::map<std::pair<size_t, size_t>, std::vector<std::vector<String>>> m_tables;

	};
} // namespace utils::file_io

namespace utils
{
	using CsvFileWriter = file_io::CsvFileWriter;
}
