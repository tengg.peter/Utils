//own
#include "Utils/Source/FileIO/FileIO.h"
#include "Utils/Source/StringOperations/StringOperations.h"
//std
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>

namespace utils::file_io
{
	using namespace utils;
	using namespace utils::string_operations;

	using std::vector;

	vector<vector<String>> GetLinesFromCsvFile(const String& fileName, const Char separator)
	{
		String line;
		FStream file(fileName); //failbit is set if the file cannot be open
		vector<vector<String>> splitLines;
		while (getline(file, line))
		{
			splitLines.push_back(Split(line, separator, false /*skipEmpty*/));
		}
		file.close();
		return splitLines;
	}

	void WriteCsvFile(const String& fileName, const vector<String>& headerLine, const vector<vector<float>>& lines, bool overWriteExisting)
	{
		FStream file;
		file.open(fileName + STR(".txt"));

		file << std::fixed;

		//print header
		for (size_t i = 0; i < headerLine.size(); i++)
		{
			file << headerLine[i];
			if (headerLine.size() > i + 1)
			{
				file << ';';
			}
		}
		file << '\n';

		for (size_t i = 0; i < lines.size(); i++)
		{
			for (size_t j = 0; j < lines[i].size(); j++)
			{
				file << lines[i][j];
				if (lines[i].size() > j + 1)
				{
					file << ';';
				}
			}
			if (lines.size() > i + 1)
			{
				file << '\n';
			}
		}

		file.close();
	}

	const vector<utils::String> ListFilesInFolder(const utils::String& folderName)
	{
		using namespace std::filesystem;
		vector<utils::String> retVector;
		for (const directory_entry& entry : directory_iterator(folderName))
		{
			if (is_regular_file(entry.status()))
			{
#ifdef _WIN32
				retVector.push_back(entry.path().filename().wstring());
#else
				retVector.push_back(entry.path().filename().string());
#endif
			}
		}
		return retVector;
	}
} // namespace utils::file_io