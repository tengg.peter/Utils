#include "Utils/Source/StringOperations/StringOperations.h"
#include "Utils/Source/FileIO/CsvFileWriter.h"

namespace utils::file_io
{
	void CsvFileWriter::AddTable(size_t row, size_t col, const std::vector<std::vector<String>>& table)
	{
		m_tables[std::pair{row, col}] = table;
	}

	void CsvFileWriter::ClearAllTables()
	{
		m_tables.clear();
	}

	void CsvFileWriter::PrintTables()
	{
		ThrowIfNotOk();
		std::vector<std::vector<const String*>> wholeTable;
		for (const auto& [startPos, table] : m_tables)
		{
			const auto& [startRow, startCol] = startPos;
			auto bigI = [startRow = startRow](size_t i) { return startRow + i; };
			auto bigJ = [startCol = startCol](size_t j) { return startCol + j; };
			for (size_t i = 0; i < table.size(); i++)
			{
				//add extra lines if I is outside
				while (wholeTable.size() <= bigI(i))
				{
					if (wholeTable.size() < bigI(i))
					{
						wholeTable.emplace_back();
					}
					else if (wholeTable.size() == bigI(i))
					{
						wholeTable.emplace_back(startCol + table[i].size(), nullptr);
					}
				}
				for (size_t j = 0; j < table[i].size(); j++)
				{
					if (wholeTable[bigI(i)].size() <= bigJ(j))
					{
						//add extra columns if J is outside
						if (wholeTable[bigI(i)].size() <= bigJ(j))
						{
							wholeTable[bigI(i)].resize(bigJ(j) + 1, nullptr);
						}
					}
					wholeTable[bigI(i)][bigJ(j)] = &table[i][j];
				}
			}
		}

		for (const auto& line : wholeTable)
		{
			for (const String* pField : line)
			{
				Write(nullptr != pField ? *pField : String{});
			}
			WriteLine();
		}
	}
} // namespace utils::file_io