#pragma once

#include "Utils/Source/Defines.h"
#include <vector>

namespace utils::file_io
{
	std::vector<std::vector<String>> GetLinesFromCsvFile(const String& fileName, const Char separator);

	void WriteCsvFile(const String& fileName, const std::vector<String>& headerLine, const std::vector<std::vector<float>>& lines, bool overWriteExisting = false);

	const std::vector<utils::String> ListFilesInFolder(const utils::String& folderName);
}