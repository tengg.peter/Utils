#pragma once
//own
#include "Utils/Source/StringOperations/StringConversion.h"
//std
#include <iosfwd>

namespace utils::enums
{
	class StringEnum
	{
	public:
		const std::string ToStdString() const { return utils::string_conversion::UtilsStringToStdString(m_enumValue); }
		const String& ToUtilsString() const { return m_enumValue; }
		StringEnum& operator=(const StringEnum& other);
#ifdef _WIN32
		operator const char* () const { return ToStdString().c_str(); }
#endif
		operator const Char* () const { return m_enumValue.c_str(); }

		virtual ~StringEnum() {}

	protected:
		String m_enumValue;

		explicit StringEnum(const String& enumValue)
		    : m_enumValue(enumValue)
		{
		}

		friend OStream& operator<<(OStream& os, const StringEnum& enumValue);
		friend bool operator==(const StringEnum& a, const StringEnum& b);
		friend bool operator<(const StringEnum& a, const StringEnum& b);
	};

	OStream& operator<<(OStream& os, const StringEnum& enumValue);
	bool operator==(const StringEnum& a, const StringEnum& b);
	bool operator<(const StringEnum& a, const StringEnum& b);
}; // namespace utils::enums

namespace utils
{
	using StringEnum = enums::StringEnum;
}