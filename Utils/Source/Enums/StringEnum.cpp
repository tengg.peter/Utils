//own
#include "StringEnum.h"
//std
#include <iostream>

namespace utils::enums
{
	StringEnum & StringEnum::operator=(const StringEnum & other)
	{
		m_enumValue = other.m_enumValue;
		return *this;
	}

	OStream & operator<<(OStream & os, const StringEnum & enumValue)
	{
		os << enumValue.m_enumValue;
		return os;
	}
	bool operator==(const StringEnum & a, const StringEnum & b)
	{
		return a.m_enumValue == b.m_enumValue;
	}
	bool operator<(const StringEnum& a, const StringEnum& b)
	{
		return a.m_enumValue < b.m_enumValue;
	}
};