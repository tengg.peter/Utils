#pragma once

struct tm;

namespace utils::date_and_time
{
	class DateTime;

	/*
	Returns:
	<0 if date1 is before (less than) date2
	0 if date1 and date2 represent the same day
	>0 if date1 is after (greater than) date2
	*/
	int CompareDatesIgnoringTime(const DateTime& a, const DateTime& b);
}