//own
#include "DateHelper.h"
#include "StopWatch.h"

void utils::date_and_time::StopWatch::Start()
{
	m_begin = std::chrono::high_resolution_clock::now();
	m_enabled = true;
}

void utils::date_and_time::StopWatch::Stop()
{
	m_end = std::chrono::high_resolution_clock::now();
	m_enabled = false;
}

void utils::date_and_time::StopWatch::Restart()
{
	Start();
}

utils::String utils::date_and_time::StopWatch::ElapsedTime() const
{
	return SecsToTime(Elapsed<std::chrono::seconds>());
}
