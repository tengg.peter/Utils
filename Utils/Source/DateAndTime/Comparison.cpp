//own
#include "Comparison.h"
#include "DateTime.h"
//std
#include <time.h>

namespace utils::date_and_time
{
	int CompareDatesIgnoringTime(const DateTime & a, const DateTime & b)
	{
		int yearDiff = a.year - b.year;
		if (0 != yearDiff)
		{
			return yearDiff;
		}
		int monthDiff = a.month - b.month;
		if (0 != monthDiff)
		{
			return monthDiff;
		}
		int dayDiff = a.day - b.day;
		if (0 != dayDiff)
		{
			return dayDiff;
		}
		return 0;
	}
}