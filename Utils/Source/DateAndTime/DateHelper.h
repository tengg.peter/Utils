#pragma once
//own
#include "DateTime.h"
#include "Utils/Source/Defines.h"
//std
#include <ctime>

namespace utils::date_and_time
{
	String SecsToTime(int secs);
}

namespace utils
{
	String MonthToString(int month);
	String MonthToString(const DateTime& dateTime);
}