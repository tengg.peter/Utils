#include "DateTime.h"
//std
#include <ctime>

namespace utils::date_and_time
{
	DateTime DateTime::Today()
	{
		time_t t = time(0);
		tm* pNow = localtime(&t);

		DateTime today;
		today.year = pNow->tm_year + 1900;
		today.month = pNow->tm_mon + 1;
		today.day = pNow->tm_mday;

		return today;
	}
	DateTime DateTime::Now()
	{
		time_t t = time(0);
		tm* pNow = localtime(&t);
		DateTime now;

		now.year = pNow->tm_year + 1900;
		now.month = pNow->tm_mon + 1;
		now.day = pNow->tm_mday;
		now.hour = pNow->tm_hour;
		now.minute = pNow->tm_min;
		now.second = pNow->tm_sec;

		return now;
	}

	DateTime::DateTime(int year, int month, int day, int hour, int minute, int second)
		: year(year), month(month), day(day), hour(hour), minute(minute), second(second)
	{
	}

	String DateTime::ToDateTimeString() const
	{
		const size_t length = 20;
		Char buff[length] = { 0 };
		STNPRINTF(buff, length, STR("%d.%02d.%02d %02d.%02d.%02d"), year, month, day, hour, minute, second);

		return String(buff);
	}
	String DateTime::ToDateString() const
	{
		const size_t length = 11;
		Char buff[length] = { 0 };
		STNPRINTF(buff, length, STR("%d.%02d.%02d"), year, month, day);

		return String(buff);
	}
	//YYYY-MM-DD
	std::string DateTime::ToSQLiteDateFormat() const
	{
		const size_t length = 20;
		char buff[length] = { 0 };
		snprintf(buff, length, "%4d-%02d-%02d", year, month, day);

		return std::string(buff);
	}
	//YYYY-MM-DD HH:MM:SS
	std::string DateTime::ToSQLiteDateTimeFormat() const
	{
		const size_t length = 20;
		char buff[length] = { 0 };
		snprintf(buff, length, "%4d-%02d-%02d %02d:%02d:%02d", year, month, day, hour, minute, second);

		return std::string(buff);
	}

	time_t DateTime::ToUnixTimestamp() const
	{
		//source: https://stackoverflow.com/questions/21679056/convert-date-to-unix-time-stamp-in-c
		time_t rawtime;
		struct tm timeInfo;

		/* get current timeinfo: */
		time(&rawtime); //or: rawtime = time(0);
		/* convert to struct: */
		timeInfo = *gmtime(&rawtime);

		/* now modify the timeinfo to the given date: */
		timeInfo.tm_year = year - 1900;
		timeInfo.tm_mon = month - 1;    //months since January - [0,11]
		timeInfo.tm_mday = day;          //day of the month - [1,31] 
		timeInfo.tm_hour = hour;         //hours since midnight - [0,23]
		timeInfo.tm_min = minute;          //minutes after the hour - [0,59]
		timeInfo.tm_sec = second;          //seconds after the minute - [0,59]

		/* call mktime: create unix time stamp from timeinfo struct */
#ifdef _WIN32
		return _mkgmtime(&timeInfo);
#elif __linux__
		return timegm(&timeInfo);
#endif
	}

	DateTime DateTime::FromString(const String& s)
	{
		DateTime dt;
		STSCANF(s.c_str(), STR("%d.%d.%d"), &dt.year, &dt.month, &dt.day);
		return dt;
	}
	DateTime DateTime::FromSQLiteDateFormat(const std::string& dateString)
	{
		DateTime dt;
		sscanf(dateString.c_str(), "%d-%d-%d %d:%d:%d", &dt.year, &dt.month, &dt.day, &dt.hour, &dt.minute, &dt.second);
		return dt;
	}

	DateTime DateTime::FromUnixTimestamp(time_t timestamp)
	{
		timestamp = timestamp > 9999999999 ? timestamp / 1000 : timestamp;	//secs from millisecs
		tm* pTm = gmtime(&(timestamp));

		DateTime dt;
		dt.year = pTm->tm_year + 1900;
		dt.month = pTm->tm_mon + 1;
		dt.day = pTm->tm_mday;
		dt.hour = pTm->tm_hour;
		dt.minute = pTm->tm_min;
		dt.second = pTm->tm_sec;

		return dt;
	}
} // namespace utils::date_and_time