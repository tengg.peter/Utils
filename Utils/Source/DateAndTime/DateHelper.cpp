//own
#include "Utils/Source/DateAndTime/DateHelper.h"
//std
#include <iomanip>
#include <ios>

namespace utils::date_and_time
{
	String SecsToTime(int secs)
	{
		int hours = secs / 3600;
		int minutes = (secs % 3600) / 60;
		secs = (secs % 3600) % 60;
		StringStream ss;
		ss << 
			std::setw(2) << std::setfill(STR('0')) << hours << ":" <<
			std::setw(2) << std::setfill(STR('0')) << minutes << ":" << 
			std::setw(2) << std::setfill(STR('0')) << secs;
		return ss.str();
	}
}

namespace utils
{
	String MonthToString(int month)
	{
		switch (month)
		{
		case 1:
			return STR("Jan");
		case 2:
			return STR("Feb");
		case 3:
			return STR("Mar");
		case 4:
			return STR("Apr");
		case 5:
			return STR("May");
		case 6:
			return STR("Jun");
		case 7:
			return STR("Jul");
		case 8:
			return STR("Aug");
		case 9:
			return STR("Sep");
		case 10:
			return STR("Oct");
		case 11:
			return STR("Nov");
		case 12:
			return STR("Dec");
		default:
			throw std::invalid_argument("month");
		}
	}

	String MonthToString(const DateTime& dateTime)
	{
		return MonthToString(dateTime.month);
	}
}