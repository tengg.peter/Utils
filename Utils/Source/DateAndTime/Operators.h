#pragma once
#include "Utils/Source/Defines.h"

//struct tm;

namespace utils::date_and_time
{
	class DateTime;

	OStream& operator<<(OStream& os, const DateTime& date);

	bool operator==(const DateTime& a, const DateTime& b);
	bool operator!=(const DateTime& a, const DateTime& b);
	bool operator<(const DateTime& a, const DateTime& b);
	bool operator>(const DateTime& a, const DateTime& b);
	bool operator<=(const DateTime& a, const DateTime& b);
	bool operator>=(const DateTime& a, const DateTime& b);

	String operator+(String s, const DateTime& date);
}