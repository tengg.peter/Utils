#pragma once
#include "Utils/Source/Defines.h"
#include "Utils/Source/StringOperations/StringConversion.h"

#ifdef _WIN32
#include <format>
#elif __linux__
#include "../3rdParty/fmt-8.1.1/include/fmt/core.h"
#include "../3rdParty/fmt-8.1.1/include/fmt/xchar.h"
#endif

namespace utils::date_and_time
{
	class DateTime
	{
	public:
		static DateTime Today();
		static DateTime Now();

		static DateTime FromString(const String& s);
		static DateTime FromSQLiteDateFormat(const std::string& dateString);
		//timeStamp in seconds
		static DateTime FromUnixTimestamp(time_t timestamp);

	public:
		explicit DateTime() = default;
		DateTime(int year, int month, int day, int hour, int minute, int second);
		virtual ~DateTime() = default;

		int year = 0;
		int month = 0;
		int day = 0;
		int hour = 0;
		int minute = 0;
		int second = 0;

		//YYYY.MM.DD HH.mm.ss
		String ToDateTimeString() const;
		String ToDateString() const;
		//YYYY-MM-DD
		std::string ToSQLiteDateFormat() const;
		//YYYY-MM-DD HH:mm:SS
		std::string ToSQLiteDateTimeFormat() const;
		time_t ToUnixTimestamp() const;
	};
}	//namespace utils::date_and_time

namespace utils
{
	using DateTime = date_and_time::DateTime;
}

#ifdef _WIN32

template<typename TChar>
struct std::formatter<utils::DateTime, TChar>
{
	constexpr auto parse(std::basic_format_parse_context<TChar>& ctx)
	{
		return std::end(ctx);
	}

	template<typename TOut>
	auto format(const utils::DateTime& dt, std::basic_format_context<TOut, TChar>& ctx)
	{
		auto&& out = ctx.out();
		if constexpr (std::is_same<char, TChar>::value)
		{
			return std::format_to(out, "{}", utils::string_conversion::UtilsStringToStdString(dt.ToDateTimeString()));
		}
		else
		{
			return std::format_to(out, L"{}", utils::string_conversion::UtilsStringToWstring(dt.ToDateTimeString()));
		}
	}
};

#elif __linux__

template <typename TChar>
struct fmt::formatter<utils::DateTime, TChar>
{
	constexpr auto parse(basic_format_parse_context<TChar>& ctx) -> decltype(ctx.begin())
	{
		return ctx.end();
	}

	template<typename TFormatContext>
	auto format(const utils::DateTime& dt, TFormatContext& ctx) -> decltype(ctx.out())
	{
		if constexpr (std::is_same<char, TChar>::value)
		{
			return format_to(ctx.out(), "{}", utils::string_conversion::UtilsStringToStdString(dt.ToDateTimeString()));
		}
		else
		{
			return format_to(ctx.out(), L"{}", utils::string_conversion::UtilsStringToWstring(dt.ToDateTimeString()));
		}
	}
};

#endif