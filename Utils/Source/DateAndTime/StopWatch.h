#pragma once
#include "Utils/Source/Defines.h"

#include <chrono>

namespace utils::date_and_time
{
	class StopWatch
	{
	public:
		StopWatch() = default;

		void Start();
		void Stop();
		void Restart();
		bool Enabled() const { return m_enabled; }

		template <typename TUnit>
		int Elapsed() const
		{
			if (m_enabled)
			{
				//TODO: remove .count(), see https://stackoverflow.com/questions/31552193/difference-between-steady-clock-vs-system-clock
				return (int)std::chrono::duration_cast<TUnit>(std::chrono::high_resolution_clock::now() - m_begin).count();
			}
			return (int)std::chrono::duration_cast<TUnit>(m_end - m_begin).count();
		}

		utils::String ElapsedTime() const;

	private:
		bool m_enabled = false;
		std::chrono::high_resolution_clock::time_point m_begin, m_end;
	};
} // namespace utils::date_and_time


namespace utils
{
	using StopWatch = date_and_time::StopWatch;
	using Timer = date_and_time::StopWatch;
}