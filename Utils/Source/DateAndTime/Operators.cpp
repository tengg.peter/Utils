//own
#include "Utils/Source/DateAndTime/DateTime.h"
#include "Utils/Source/DateAndTime/Operators.h"
//std
#include <iomanip>
#include <string>

namespace utils::date_and_time
{
	OStream& operator<<(OStream& os, const DateTime& date)
	{
		return os << date.ToDateTimeString();
	}
	bool operator==(const DateTime& a, const DateTime& b)
	{
		return a.year == b.year &&
		       a.month == b.month &&
		       a.day == b.day &&
		       a.hour == b.hour &&
		       a.minute == b.minute &&
		       a.second == b.second;
	}
	bool operator!=(const DateTime& a, const DateTime& b)
	{
		return !(a == b);
	}
	bool operator<(const DateTime& a, const DateTime& b)
	{
		return a.year < b.year ? true : a.year > b.year ? false : 
			a.month < b.month ? true : a.month > b.month ? false : 
			a.day < b.day ? true : a.day > b.day ? false : 
			a.hour < b.hour ? true : a.hour > b.hour ? false : 
			a.minute < b.minute ? true : a.minute > b.minute ? false : 
			a.second < b.second ? true : 
			false;
	}

	bool operator>(const DateTime& a, const DateTime& b)
	{
		return !(a <= b);
	}

	bool operator<=(const DateTime& a, const DateTime& b)
	{
		return a < b || a == b;
	}

	bool operator>=(const DateTime& a, const DateTime& b)
	{
		return !(a < b);
	}

	String operator+(String s, const DateTime& dt)
	{
		s += dt.ToDateTimeString();
		return s;
	}
} // namespace utils::date_and_time