//own
#include "RandomService.h"
//std
#include <chrono>
#include <iterator>
#include <numeric>
#include <stdexcept>

namespace utils
{
	RandomService::RandomService()
	{
		m_engine.seed(std::chrono::high_resolution_clock::now().time_since_epoch().count());
	}

	RandomService& RandomService::GetInstance()
	{
		static RandomService instance;
		return instance;
	}

	float RandomService::GetUniformFloat(float min, float max)
	{
		if (m_realDistribution.a() != min || m_realDistribution.b() != max)
		{
			m_realDistribution.param(std::uniform_real_distribution<double>::param_type(min, max));
		}

		return static_cast<float>(m_realDistribution(m_engine));
	}

	double RandomService::GetUniformDouble(double min, double max)
	{
		if (m_realDistribution.a() != min || m_realDistribution.b() != max)
		{
			m_realDistribution.param(std::uniform_real_distribution<double>::param_type(min, max));
		}

		return m_realDistribution(m_engine);
	}

	int RandomService::GetUniformInt(int min, int max)
	{
		if (m_intDistribution.a() != min || m_intDistribution.b() != max)
		{
			m_intDistribution.param(std::uniform_int_distribution<int>::param_type(min, max));
		}

		return m_intDistribution(m_engine);
	}

	double RandomService::GetGaussianDouble(double mean, double standardDevation)
	{
		if (m_normalDistribution.mean() != mean || m_normalDistribution.stddev() != standardDevation)
		{
			m_normalDistribution.param(std::normal_distribution<double>::param_type(mean, standardDevation));
		}

		return m_normalDistribution(m_engine);
	}

	bool RandomService::GetUniformBool()
	{
		return 1 == m_boolDistribution(m_engine);
	}

	std::vector<size_t> RandomService::GetSample(size_t first, size_t last, size_t sampleSize)
	{
		if (first > last)
		{
			throw std::invalid_argument("Last must be greater or equal than first.");
		}

		static std::vector<size_t> numbers;
		if (numbers.empty() || numbers.front() != first || numbers.back() != last)
		{
			numbers.resize(last - first + 1);
			std::iota(numbers.begin(), numbers.end(), first);
		}
		std::vector<size_t> sample;
		std::sample(numbers.cbegin(), numbers.cend(), std::back_inserter(sample), sampleSize, m_engine);
		return sample;
	}

	void RandomService::Seed(uint64_t seed)
	{
		m_engine.seed(seed);
	}
} // namespace utils