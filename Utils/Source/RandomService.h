#pragma once
#include <algorithm>
#include <random>

namespace utils
{
	class RandomService
	{
	public:
		RandomService(const RandomService&) = delete;
		void operator=(const RandomService&) = delete;

		static RandomService& GetInstance();

		template<typename TNum>
		TNum GetUniform(TNum min, TNum max)
		{
			if constexpr (std::is_same<double, TNum>::value)
			{
				return GetUniformDouble(min, max);
			}
			else if constexpr (std::is_same<float, TNum>::value)
			{
				return GetUniformFloat(min, max);
			}
			else if constexpr (std::is_same<int, TNum>::value)
			{
				return GetUniformInt(min, max);
			}
			else if constexpr (std::is_same<bool, TNum>::value)
			{
				return GetUniformBool();
			}
			else
			{
				[]<bool flag = false>()
            		{ static_assert(flag, "Type not supported."); }();
			}
		}

		double GetUniformDouble(double min, double max);
		float GetUniformFloat(float min, float max );
		int GetUniformInt(int min, int max);
		double GetGaussianDouble(double mean, double standardDevation);
		bool GetUniformBool();
		std::vector<size_t> GetSample(size_t first, size_t last, size_t sampleSize);

		std::mt19937_64& GetRandomEngine() { return m_engine; }
		void Seed(uint64_t seed);

		template<class T>
		void Shuffle(std::vector<T>& vec) { std::shuffle(vec.begin(), vec.end(), m_engine); }

		template<class T>
		void ShuffleWithSeed(std::vector<T>& vec, uint32_t seed)
		{
			std::srand(seed);
			std::shuffle(vec.begin(), vec.end(), m_engine);
		}

		//returns a random index from the container with respect to the input weights specified
		template<class TContainer>
		size_t SpinTheRouletteWheel(const TContainer& weights)
		{
			std::vector<size_t> i;
			size_t iCnt = 0;

			for(auto it = weights.cbegin(); it != weights.cend(); ++it)
			{
				i.push_back(iCnt++);
			}
			i.push_back(iCnt);

			std::piecewise_constant_distribution wheel(i.begin(), i.end(), weights.begin());
			return static_cast<size_t>(wheel(m_engine));
		}

	private:
		std::mt19937_64 m_engine;

		std::uniform_real_distribution<double> m_realDistribution;
		std::uniform_int_distribution<int> m_intDistribution;
		std::normal_distribution<double> m_normalDistribution;
		std::uniform_int_distribution<int> m_boolDistribution{0,1};

		RandomService();
	};
}