#pragma once

#include <exception>
#include <string>

namespace utils
{
    template<typename T>
    void AssertEquals(const T& a, const T& b, const std::string exceptionMessage)
    {
        if (a != b)
        {
            throw std::runtime_error(exceptionMessage);
        }
    }
}