#pragma once
#include "Defines.h"

namespace utils
{
	// https://stackoverflow.com/questions/30184998/how-to-disable-cout-output-in-the-runtime
	// https://stackoverflow.com/questions/5419356/redirect-stdout-stderr-to-a-string

	class MuteCout
	{
	public:
		explicit MuteCout(bool disableCout) : m_disableCout(disableCout)
		{
			if (m_disableCout)
			{
				TCOUT.setstate(std::ios_base::failbit);
			}
		}

		virtual ~MuteCout()
		{
			if (m_disableCout)
			{
				TCOUT.clear();
			}
		}

	private:
		bool m_disableCout;
	};

	class RedirectCout 
	{
	public:
		explicit RedirectCout(utils::StreamBuf* newBuffer) : old(TCOUT.rdbuf(newBuffer))
		{}

		~RedirectCout()
		{
			TCOUT.rdbuf(old);
		}

	private:
		utils::StreamBuf* old;
	};
}