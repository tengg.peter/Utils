#include <fstream>
#include <iostream>
#include <sstream>
#include <typeinfo>

namespace utils
{
#ifdef _WIN32
	using Char = wchar_t;
#define STR(s) L##s
#define TCOUT std::wcout
#define TCIN std::wcin
#define STNPRINTF swprintf
#define STSCANF swscanf
#elif __linux__
	using Char = char;
#define STR(s) s
#define TCOUT std::cout
#define TCIN std::cin
#define STNPRINTF snprintf
#define STSCANF sscanf
#else
#error "OS not supported"
#endif
	using FStream = std::basic_fstream<Char>;
	using IfStream = std::basic_ifstream<Char>;
	using OfStream = std::basic_ofstream<Char>;
	using OStream = std::basic_ostream<Char>;
	using StreamBuf = std::basic_streambuf<Char>;
	using String = std::basic_string<Char>;
	using StringStream = std::basic_stringstream<Char>;
} // namespace utils
