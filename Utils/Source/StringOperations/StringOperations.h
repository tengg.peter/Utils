#pragma once

#include "Utils/Source/Defines.h"

#ifdef __linux__
#include <algorithm>
#endif
#include <memory>
#include <time.h>
#include <vector>

namespace utils::string_operations
{
	namespace
	{
		template <typename TChar>
		std::vector<std::basic_string<TChar>>& SplitImpl(
		    const std::basic_string<TChar>& s,
		    TChar delim,
		    std::vector<std::basic_string<TChar>>& elems,
		    bool skipEmpty)
		{
			std::basic_stringstream<TChar> ss(s);
			std::basic_string<TChar> item;
			while (getline(ss, item, delim))
			{
				if (!skipEmpty || !item.empty())
					elems.emplace_back(item);
			}
			if (!skipEmpty && s.back() == delim)
			{
				elems.emplace_back();
			}

			return elems;
		}
	} // namespace

	template <typename TChar>
	std::vector<std::basic_string<TChar>> Split(const std::basic_string<TChar>& s, TChar delim, bool skipEmpty)
	{
		std::vector<std::basic_string<TChar>> elems;
		SplitImpl(s, delim, elems, skipEmpty);
		return elems;
	}

	template <typename TChar>
	std::vector<std::basic_string<TChar>> Split(const std::basic_string<TChar>& s, const TChar* delimiter, bool skipEmpty = true)
	{
		//code from Stack Overflow: https://stackoverflow.com/a/14266139
		std::vector<std::basic_string<TChar>> retVector;

		size_t last = 0;
		size_t next = 0;
		size_t delimLegth = std::char_traits<TChar>::length(delimiter);
		while ((next = s.find(delimiter, last)) != std::string::npos)
		{
			std::basic_string<TChar> sub = s.substr(last, next - last);
			if (!skipEmpty || !sub.empty())
			{
				retVector.push_back(sub);
			}
			last = next + delimLegth;
		}
		std::basic_string<TChar> sub = s.substr(last);
		if (!skipEmpty || !sub.empty())
		{
			retVector.push_back(sub);
		}
		return retVector;
	}

	template <typename TChar>
	bool Contains(const std::basic_string<TChar>& str, const std::basic_string<TChar>& substring)
	{
		return std::basic_string<TChar>::npos != str.find(substring);
	}

	template <typename TChar>
	bool Contains(const std::basic_string<TChar>& str, const TChar* substring)
	{
		return std::basic_string<TChar>::npos != str.find(substring);
	}


	template <typename TChar>
	std::vector<std::basic_string<TChar>> SplitByEach(
	    const std::basic_string<TChar>& str, 
		const std::vector<std::basic_string<TChar>>& delimiters,
		bool skipEmpty = true,
		bool includeDelims = false)
	{
		std::vector<std::basic_string<TChar>> v1{str};
		std::vector<std::basic_string<TChar>> v2;
		std::vector<std::basic_string<TChar>>* pCurr = &v1;
		std::vector<std::basic_string<TChar>>* pNext = &v2;

		for (const std::basic_string<TChar>& delim : delimiters)
		{
			pNext->clear();
			for (const std::basic_string<TChar>& currString : *pCurr)
			{
				if (!Contains(currString, delim))
				{
					pNext->push_back(currString);
					continue;
				}

				//https://stackoverflow.com/a/14266139
				size_t last = 0;
				size_t next = 0;
				while ((next = currString.find(delim, last)) != std::string::npos)
				{
					std::basic_string<TChar> sub = currString.substr(last, next - last);
					if (!skipEmpty || !sub.empty())
					{
						pNext->push_back(sub);
					}
					if (includeDelims)
					{
						pNext->push_back(delim);
					}
					last = next + delim.length();
				}
				std::basic_string<TChar> sub = currString.substr(last);
				if (!skipEmpty || !sub.empty())
				{
					pNext->push_back(sub);
				}
			}
			std::swap(pNext, pCurr);
		}

		return *pCurr;
	}

	template <typename TChar>
	std::basic_string<TChar> RemoveCharacters(const std::basic_string<TChar>& removeFromThis, const TChar* characters)
	{
		std::basic_string<TChar> retString = removeFromThis;
		size_t len = std::char_traits<TChar>::length(characters);
		for (int i = 0; i < len; i++)
		{
			size_t pos = retString.npos;
			do
			{
				pos = retString.find(characters[i]);
				if (retString.npos == pos)
				{
					break;
				}
				retString.erase(pos, 1);
			} while (true);
		}
		return retString;
	}

	template <typename TChar>
	std::basic_string<TChar> Replace(const std::basic_string<TChar>& str, const TChar* replaceThis, const TChar* withThis)
	{
		std::basic_string<TChar> returnString = str; //makes a local copy. We don't touch the original string

		size_t pos = 0;
		size_t oldPatternLen = std::char_traits<TChar>::length(replaceThis);
		size_t newPatternLen = std::char_traits<TChar>::length(withThis);
		do
		{
			pos = returnString.find(replaceThis, pos);
			if (std::basic_string<TChar>::npos == pos)
			{
				break;
			}
			returnString.replace(pos, oldPatternLen, withThis);
			pos += newPatternLen;
		} while (true);

		return returnString;
	}


	template <typename TChar>
	bool IsAlphanumeric(const std::basic_string<TChar>& s)
	{
		for (TChar c : s)
		{
			if (!isalnum(c))
			{
				return false;
			}
		}
		return true;
	}

	[[deprecated("Use utils::ToLower() instead")]]
	std::wstring ToLower(const std::wstring& s);
	[[deprecated("Use utils::ToLower() instead")]]
	std::string ToLower(const std::string& s);
	[[deprecated("Use utils::ToUpper() instead")]]
	std::wstring ToUpper(const std::wstring& s);
	[[deprecated("Use utils::ToUpper() instead")]]
	std::string ToUpper(const std::string& s);

	std::wstring ToLowerFirstChar(const std::wstring& s);
	std::string ToLowerFirstChar(const std::string& s);
	std::wstring ToUpperFirstChar(const std::wstring& s);
	std::string ToUpperFirstChar(const std::string& s);

	template <typename TChar>
	bool EqualsIgnoreCase(const std::basic_string<TChar>& a, const std::basic_string<TChar>& b)
	{
		return ToLower(a) == ToLower(b);
	}

	template <typename TContainer>
	String ContainerToCsvString(const TContainer& container, Char delimiter)
	{
		StringStream ss;
		for (auto it = std::cbegin(container); it != std::cend(container); ++it)
		{
			ss << *it << delimiter;
		}
		String s = ss.str();
		return s.substr(0, s.size() - 1); //removes the last delimeter
	}

	//TODO: Make it variadic, so any number of parameters will be inserted
	String InsertInfoToOutputFileName(const String& originalFileName, const String& stringToInsert);

	class Tab
	{
	public:
		explicit Tab() = default;
		explicit Tab(int count) : m_count(count) { }

		//prefix increment
		Tab& operator++();
		//prefix decrement
		Tab& operator--();

		friend std::ostream& operator<<(std::ostream& os, const Tab& tab);
	private:
		int m_count = 1;
	};

	std::ostream& operator<<(std::ostream& os, const Tab& tab);


	template<class TContainer, class TChar>
	inline std::basic_string<TChar> JoinStrings(const TContainer& strings, const std::basic_string<TChar>& delim)
	{
		std::basic_string<TChar> join;
		for (auto it = strings.cbegin(); it != strings.cend(); ++it)
		{
			if(strings.cbegin() != it)
			{
				join += delim;
			}
			join += *it;
		}
		return join;
	}

	template<class TContainer, class TChar>
	inline std::basic_string<TChar> JoinStrings(const TContainer& strings, const TChar* delim)
	{
		return JoinStrings(strings, std::basic_string<TChar>{delim});
	}

	template <typename TChar>
	std::basic_string<TChar>& LTrim(std::basic_string<TChar>& str)
	{
		auto it2 =  std::find_if(str.begin(), str.end(), 
			[](TChar ch) { return !std::isspace<TChar>(ch, std::locale::classic()); });
		str.erase(str.begin(), it2);
		return str;   
	}

	template <typename TChar>
	std::basic_string<TChar>& RTrim(std::basic_string<TChar>& str)
	{
		auto it1 =  std::find_if(str.rbegin(), str.rend(), 
			[](TChar ch) { return !std::isspace<TChar>(ch, std::locale::classic()); });
		str.erase(it1.base(), str.end());
		return str;   
	}

	template <typename TChar>
	std::basic_string<TChar>& Trim(std::basic_string<TChar>& str)
	{
		return LTrim(RTrim(str));
	}

	template <typename TChar>
	std::basic_string<TChar> TrimCopy(const std::basic_string<TChar>& str)
	{
		auto s = str;
		return LTrim(RTrim(s));
	}

	template <typename TNum>
	[[deprecated("Use utils::Commas() instead")]]
	utils::String Commas(TNum number)
	{
		utils::String stringNum = ToString(number);
		size_t commaOffset = stringNum.size() % 3;
		for (size_t i = 1; i < stringNum.size(); i++)
		{
			if (int d = i - commaOffset; d >= 0 && 0 == d % 3)
			{
				stringNum.insert(stringNum.begin() + i, STR(','));
				++i;
				commaOffset = (commaOffset + 1) % 3;
			}
		}

		return stringNum;
	}

} // namespace utils::string_operations

namespace utils
{
	template <typename TNum>
	inline utils::String Commas(TNum number)
	{
		return string_operations::Commas(number);
	}

	inline std::string ToLower(const std::string& s)
	{
		return string_operations::ToLower(s);
	}

	inline std::wstring ToLower(const std::wstring& s)
	{
		return string_operations::ToLower(s);
	}

	inline std::string ToUpper(const std::string& s)
	{
		return string_operations::ToUpper(s);
	}

	inline std::wstring ToUpper(const std::wstring& s)
	{
		return string_operations::ToUpper(s);
	}

}	//namespace utils
