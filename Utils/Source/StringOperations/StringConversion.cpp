//own
#include "StringConversion.h"
#include "StringOperations.h"
//std
#include <codecvt>
#include <exception>
#include <locale>
#ifdef _WIN32
#include <Windows.h>
#endif

namespace utils::string_conversion
{
	String FloatToUtilsString(double f, int decimals, bool forceSign, bool prependApostrophe)
	{
		Char buf[100] = { 0 };
		STNPRINTF(buf, 100, STR("%.*f"), decimals, f);
		String ret;
		if (prependApostrophe)
		{
			ret += STR('\'');
		}
		if (forceSign && f > 0.0f)
		{
			ret += STR("+");
		}
		ret += String(buf);
		return ret;
	}

#ifdef _WIN32
	std::string ToUtf8(const std::wstring& str)
	{
		int numChars = ::WideCharToMultiByte(CP_UTF8, 0, str.c_str(), static_cast<int>(str.size()), NULL, 0, NULL, NULL);
		if (0 == numChars)
		{
			return std::string();
		}

		std::string utf8String(numChars, 0);
		::WideCharToMultiByte(CP_UTF8, 0, str.c_str(), static_cast<int>(str.size()), const_cast<char*>(utf8String.c_str()), numChars, NULL, NULL);

		return utf8String;
	}
	std::wstring ToUtf16(const std::string& str)
	{
		if (str.empty())
		{
			return std::wstring();
		}

		int sz = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), 0, 0);
		std::wstring utf16String(sz, 0);
		MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &utf16String[0], sz);
		return utf16String;
	}
#else
	std::string ToUtf8(const std::string& str)
	{
		return str;
	}

	std::wstring ToUtf16(const std::string& str)
	{
		//TODO
		throw std::logic_error("Not implemented");
	}
#endif

	std::wstring StdStringToWstring(const std::string& s)
	{
		//got from https://stackoverflow.com/a/18374698
		using convert_typeX = std::codecvt_utf8<wchar_t>;
		std::wstring_convert<convert_typeX, wchar_t> converterX;

		return converterX.from_bytes(s);
	}

	std::string WstringToStdString(const std::wstring& s)
	{
		//got from https://stackoverflow.com/a/18374698
		using convert_typeX = std::codecvt_utf8<wchar_t>;
		std::wstring_convert<convert_typeX, wchar_t> converterX;

		return converterX.to_bytes(s);
	}


	std::string UtilsStringToStdString(const String& ustr)
	{
#ifdef _WIN32
		return WstringToStdString(ustr);
#else
		return ustr;
#endif
	}

	std::wstring UtilsStringToWstring(const String& str)
	{
#ifdef _WIN32
		return str;
#else
		return StdStringToWstring(str);
#endif
	}

	String StdStringToUtilsString(const std::string& str)
	{
#ifdef _WIN32
		return StdStringToWstring(str);
#else
		return str;
#endif
	}

	std::string ToSqlStringLiteral(const std::string& s)
	{
		std::string us = string_operations::Replace(s, "'", "''"); //escapes single quote in text
		return '\'' + us + '\'';
	}

	std::string ToSqlStringLiteral(const std::wstring& s)
	{
		std::wstring us = string_operations::Replace(s, L"'", L"''"); //escapes single quote in text
		return '\'' + string_conversion::WstringToStdString(us) + '\'';
	}

} // namespace utils::string_conversion

namespace utils
{
	std::wstring ToWstring(const std::string& s)
	{
		return string_conversion::StdStringToWstring(s);
	}

	std::wstring ToWstring(const std::wstring& s)
	{
		return s;
	}

	std::string ToStdString(const std::wstring& s)
	{
		return string_conversion::WstringToStdString(s);
	}

	std::string ToStdString(const std::string& s)
	{
		return s;
	}

	String ToUtilsString(const std::string& str)
	{
		return string_conversion::StdStringToUtilsString(str);
	}

}