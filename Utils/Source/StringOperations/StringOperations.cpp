//own
#include "Utils/Source/StringOperations/StringConversion.h"
#include "Utils/Source/StringOperations/StringOperations.h"
//std
#include <algorithm>

namespace utils::string_operations
{
	std::wstring ToLower(const std::wstring& s)
	{
		std::wstring retVal{s};
		std::transform(retVal.begin(), retVal.end(), retVal.begin(), ::towlower);
		return retVal;
	}

	std::string ToLower(const std::string& s)
	{
		std::string retVal{s};
		std::transform(retVal.begin(), retVal.end(), retVal.begin(), ::tolower);
		return retVal;
	}

	std::wstring ToUpper(const std::wstring& s)
	{
		std::wstring retVal{s};
		std::transform(retVal.begin(), retVal.end(), retVal.begin(), ::towupper);
		return retVal;
	}

	std::string ToUpper(const std::string& s)
	{
		std::string retVal{s};
		std::transform(retVal.begin(), retVal.end(), retVal.begin(), ::toupper);
		return retVal;
	}


	std::wstring ToLowerFirstChar(const std::wstring& s)
	{
		return { static_cast<wchar_t>(::towlower(s[0])) + s.substr(1, s.size() - 1) };
	}

	std::string ToLowerFirstChar(const std::string& s)
	{
		return {static_cast<char>(::tolower(s[0])) + s.substr(1, s.size() - 1)};
	}

	std::wstring ToUpperFirstChar(const std::wstring& s)
	{
		return {static_cast<wchar_t>(::towupper(s[0])) + s.substr(1, s.size() - 1)};
	}

	std::string ToUpperFirstChar(const std::string& s)
	{
		return {static_cast<char>(::toupper(s[0])) + s.substr(1, s.size() - 1)};
	}


	String InsertInfoToOutputFileName(const String& originalFileName, const String& stringToInsert)
	{
		StringStream ss;
		size_t dotPos = originalFileName.find_last_of(STR('.'));
		ss << originalFileName.substr(0, dotPos) << STR(" - ") << stringToInsert << STR(".") << originalFileName.substr(dotPos + 1);
		return ss.str();
	}

	Tab& Tab::operator++()
	{
		++m_count;
		return *this;
	}

	Tab& Tab::operator--()
	{
		--m_count;
		return *this;
	}

	std::ostream& operator<<(std::ostream& os, const Tab& tab)
	{
		for (size_t i = 0; i < tab.m_count; i++)
		{
			os << '\t';
		}
		return os;
	}
} // namespace utils::string_operations