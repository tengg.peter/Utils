#pragma once

//std
#ifdef _WIN32
#include <format>
#elif __linux__
#include "../3rdParty/fmt-8.1.1/include/fmt/core.h"
#include "../3rdParty/fmt-8.1.1/include/fmt/xchar.h"
#include <utility>	//std::forward
#endif
#include <string>
#include <string_view>

namespace utils
{
#ifdef _WIN32
	template<typename... Args>
	inline std::string Fmt(std::string_view fmt, Args&&... args)
	{
		return std::vformat(fmt, std::make_format_args(args...));
	}

	template<typename... Args>
	inline std::wstring Fmt(std::wstring_view fmt, Args&&... args)
	{
		return std::vformat(fmt, std::make_wformat_args(args...));
	}

#elif __linux__
	// https://developercommunity.visualstudio.com/t/error-c7595-only-with-visualstudio-2022/1650148

	//TODO: Test un Linux (just run the unit tests)
	template<typename... Args>
	std::string Fmt(fmt::format_string<Args...> format, Args&&... args) 
	{
		return fmt::format(format, std::forward<Args>(args)...);
	}

	template<typename... Args>
	std::wstring Fmt(const wchar_t* format, Args&&... args) 
	{
		return fmt::format(format, std::forward<Args>(args)...);
	}
#endif
}