#pragma once
//own
#include "Utils/Source/Defines.h"
//std
#include <ctime>

namespace utils
{
	//Wrapper around std::to_string or to_wstring, depending on the OS
	template <typename T>
	String ToString(T t)
	{
#ifdef _WIN32
		return std::to_wstring(t);
#else
		return std::to_string(t);
#endif
	}
} // namespace utils

namespace utils::string_conversion
{
	template <typename T>
	String RatioToPercent(T ratio, int decimals = 2)
	{
		Char buf[100] = {0};
		STNPRINTF(buf, 100, STR("%.*f%%"), decimals, ratio * 100);
		return String(buf);
	}

	String FloatToUtilsString(double f, int decimals = 2, bool forceSign = false, bool prependApostrophe = false);

#ifdef _WIN32
	std::string ToUtf8(const std::wstring& str);

	std::wstring ToUtf16(const std::string& str);
#else
	std::string ToUtf8(const std::string& str);

	std::wstring ToUtf16(const std::string& str);
#endif
	std::wstring StdStringToWstring(const std::string& s);
	std::string WstringToStdString(const std::wstring& s);

	//If utils::String is std::wstring, converts it to std::string. This is useful on Windows. Otherwise returns std::string
	std::string UtilsStringToStdString(const String& str);

	//If utils::String is std::wstring, returns the same string. This is useful on Linux. Otherwise converts it to std::wstring
	std::wstring UtilsStringToWstring(const String& str);

	String StdStringToUtilsString(const std::string& str);

	std::string ToSqlStringLiteral(const std::string& s);
	std::string ToSqlStringLiteral(const std::wstring& s);

	//Example conversion: "16.28%" => 16.28
	template<typename TChar>
	double PercentToDouble(const std::basic_string<TChar>& percentString)
	{
		using TString = std::basic_string<TChar>;
		TString ts;
		TChar sign;
		if constexpr (std::is_same<char, TChar>{})
		{
			sign = '%';
		}
		else if constexpr (std::is_same<wchar_t, TChar>{})
		{
			sign = L'%';
		}
		else
		{
			throw std::runtime_error("Unsupported char type");
		}

		if (percentString.back() == sign)
		{
			ts = TString(percentString.cbegin(), percentString.cend() - 1);
		}
		else
		{
			ts = percentString;
		}
		return std::stod(ts, nullptr);
	}
} // namespace utils::string_conversion

namespace utils
{
	std::wstring ToWstring(const std::string& s);
	std::wstring ToWstring(const std::wstring& s);
	std::string ToStdString(const std::wstring& s);
	std::string ToStdString(const std::string& s);
	String ToUtilsString(const std::string& str);
}