#pragma once
//own
#include "Utils/Source/DateAndTime/DateTime.h"
#include "Utils/Source/Defines.h"
#include "Utils/Source/StringOperations/StringConversion.h"
//std
#include <fstream>
#include <memory>
#include <sstream>
#include <string>


namespace utils
{
	class Logger
	{
	public:
		static void FilePath(const String& filePath);

		template <typename TString>
		static void LogError(const TString& message, bool toConsoleToo)
		{
			LogImpl(ERROR_, message, toConsoleToo);
		}

		template <typename TString>
		static void LogWarning(const TString& message, bool toConsoleToo)
		{
			LogImpl(WARNING_, message, toConsoleToo);
		}

		template <typename TString>
		static void LogInfo(const TString& message, bool toConsoleToo)
		{
			LogImpl(INFO_, message, toConsoleToo);
		}

		static void LogError(const std::exception& e, bool toConsoleToo)
		{
			LogImpl(ERROR_, e.what(), toConsoleToo);
		}

		static void LogWarning(const std::exception& e, bool toConsoleToo)
		{
			LogImpl(WARNING_, e.what(), toConsoleToo);
		}

		static void LogInfo(const std::exception& e, bool toConsoleToo)
		{
			LogImpl(INFO_, e.what(), toConsoleToo);
		}

		virtual ~Logger(); //must be public, otherwise unique_ptr cannot access it

	private:
		static const inline String ERROR_ = STR("Err");
		static const inline String WARNING_ = STR("Warn");
		static const inline String INFO_ = STR("Info");

		static std::unique_ptr<Logger> s_upInstance;
		static String s_filePath; //= STR("");

		std::ofstream m_logFile;

		static void InitIfNotInitYet();

		static String FormatLog(const String& severity, const String& message)
		{
			StringStream ss;
			ss << '[' << DateTime::Now().ToDateTimeString() << "] "<< severity << ": " << message;

			return ss.str();
		}

#ifdef _WIN32
		static String FormatLog(const String& severity, const std::string& message)
		{
			return FormatLog(severity, string_conversion::StdStringToUtilsString(message));
		}
#endif

		template <typename TString>
		static void LogImpl(const String& severity, const TString& message, bool toConsoleToo)
		{
			InitIfNotInitYet();
			String log = FormatLog(severity, message);
			s_upInstance->Log(log, toConsoleToo);
		}
		void Log(const String& log, bool toConsoleToo);

		explicit Logger(const String& filePath);
	};
} // namespace utils