//own
#include "Utils/Source/Logging/Logger.h"
#include "Utils/Source/StringOperations/StringConversion.h"
//std
#include <filesystem>
#include <iostream>

namespace utils
{
	std::unique_ptr<Logger> Logger::s_upInstance;
	String Logger::s_filePath = STR("");

	Logger::Logger(const String& filePath)
	{
		m_logFile.open(filePath);
	}

	void Logger::FilePath(const String& filePath)
	{
		s_filePath = filePath;
	}

	Logger::~Logger()
	{
		m_logFile.close();
	}

	void Logger::InitIfNotInitYet()
	{
		if (s_upInstance)
		{
			return;
		}

		if (s_filePath.empty())
		{
			s_filePath = STR("Log - ") + DateTime::Now().ToDateTimeString() + STR(".txt");
		}
		else
		{
			const String targetDir = s_filePath.substr(0, s_filePath.find_last_of(STR('/')));
			std::filesystem::create_directories(targetDir);
			s_filePath += STR(" - ") + DateTime::Now().ToDateTimeString() + STR(".txt");
		}

		s_upInstance.reset(new Logger(s_filePath));
	}

	void Logger::Log(const String& log, bool toConsoleToo)
	{
		using std::endl;
		if (toConsoleToo)
		{
			TCOUT << log << endl;
		}
		m_logFile << string_conversion::ToUtf8(log) << endl;
	}
} // namespace utils