//own
#include "Utils/Source/Threading/Threading.h"
//std
#include <map>
#include <mutex>
#include <thread>

namespace utils::threading
{
	namespace
	{
		std::map<std::thread::id, int> g_threadNumberRegister;
		std::mutex g_threadNumberMutex;
		int g_nextThreadNumber = 0;
	} // namespace

	int MyThreadNumber()
	{
		std::lock_guard guard(g_threadNumberMutex);
		auto ret = g_threadNumberRegister.insert(std::make_pair(std::this_thread::get_id(), g_nextThreadNumber));
		if (ret.second)
		{
			++g_nextThreadNumber;
		}
		return ret.first->second;
	}

	int AcquireNewThreadNumber()
	{
		std::lock_guard guard(g_threadNumberMutex);
		int nextThreadNumber = g_nextThreadNumber++;
		g_threadNumberRegister[std::this_thread::get_id()] = nextThreadNumber++;
		return nextThreadNumber;
	}
} // namespace utils::threading