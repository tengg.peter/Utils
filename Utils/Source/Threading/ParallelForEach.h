#pragma once

#include <atomic>
#include <future>
#include <vector>

namespace utils
{
	namespace impl
	{
		inline const std::atomic_bool* pIsAborting = nullptr;
		inline bool IsAborting()
		{
			return pIsAborting && pIsAborting->load();
		}
	}

	// numThreadsHint = 0 tries to use the maximum number, which is n-2, where n is the number of logical threads.
	template<typename TItem, typename TFunc>
	void ParallelForEach(std::vector<TItem>& items, TFunc func, uint16_t numThreadsHint = 0, const std::atomic_bool const* pAbort = nullptr)
	{
		if (items.empty())
		{
			return;
		}
		impl::pIsAborting = pAbort;
		if (impl::IsAborting())
		{
			return;
		}
		if (1 == items.size())
		{
			func(items.front());
			return;
		}

		size_t completed = 0;
		std::vector<std::future<TItem&>> futures;
		const uint16_t hwConc = static_cast<uint16_t>(std::thread::hardware_concurrency());
		numThreadsHint = numThreadsHint == 0 ? hwConc : numThreadsHint;
		const uint16_t maxThreads = hwConc > 2 ? hwConc - 2 : 1;
		const uint16_t numThreads = std::min(maxThreads, numThreadsHint);

		std::vector<bool> done(items.size(), false);

		size_t totalThreadsStarted = 0;
		do
		{
			using std::endl;
			//starting new tasks
			while (!impl::IsAborting() && futures.size() < numThreads)
			{
				if (items.size() <= totalThreadsStarted)
				{
					break;
				}
				auto& item = items[totalThreadsStarted];
				if (!done[totalThreadsStarted])
				{
					futures.push_back(std::async(std::launch::async,
						[func, &done, totalThreadsStarted](TItem& item) -> TItem&
						{
							func(item);
							done[totalThreadsStarted] = true;
							return item;
						},
						std::ref(item)));
				}
				else
				{
					++completed;
				}
				++totalThreadsStarted;
			}
			//checking tasks that have been finished, so new ones can be started
			using namespace std::chrono_literals;
			for (auto it = futures.begin(); futures.end() != it; ++it)
			{
				if (std::future_status::ready == it->wait_for(0ms))
				{
					it = futures.erase(it);
					++completed;
				}
				if (futures.end() == it)
				{
					break;
				}
			}
		} while (!impl::IsAborting() && completed < items.size());

		if (impl::IsAborting())
		{
			//we need to wait for all tasks to finish before cleaning up
			for (auto& task : futures)
			{
				task.wait();
			}
		}
	}
}