#pragma once

namespace utils::threading
{
	// If this thread doesn't have a number yet, it gets one and returns it. 
	// Otherwise returns the existing one. This will always return the same number for the same thread
	int MyThreadNumber();
	// Gets a new number for this thread, regardless of it had one previously. It always returns a new number.
	int AcquireNewThreadNumber();
}