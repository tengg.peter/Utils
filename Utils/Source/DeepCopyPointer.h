#pragma once

#include <memory>

namespace utils
{
	template<typename TObj, typename TCloneFunc = TObj * (*)(const TObj&)>
	class DeepCopyPointer
	{

	public:
		using CloneFuncType = TCloneFunc;

		//create
		explicit DeepCopyPointer()
		{
			static_assert(!std::is_abstract<TObj>::value, "default ctor(): TObj is an abstract class. You must specify a cloning function for it.");
		}
		explicit DeepCopyPointer(TObj* object)
		{
			static_assert(!std::is_abstract<TObj>::value, "ctor(TObj*): TObj is an abstract class. You must specify a cloning function for it.");
			m_upObject.reset(object);
		}
		explicit DeepCopyPointer(TCloneFunc cloneFunc)
			: m_cloneFunc(cloneFunc), m_cloneFuncProvided(true)
		{ }
		explicit DeepCopyPointer(TObj* object, TCloneFunc cloneFunc) : DeepCopyPointer(cloneFunc)
		{
			m_upObject.reset(object);
		}

		//copy:
		DeepCopyPointer(const DeepCopyPointer<TObj, TCloneFunc>& other) :
			m_cloneFunc(other.m_cloneFunc),
			m_cloneFuncProvided(other.m_cloneFuncProvided)
		{
			CopyObject(other);
		}
		DeepCopyPointer<TObj, TCloneFunc>& operator=(const DeepCopyPointer<TObj, TCloneFunc>& other)
		{
			m_cloneFunc = other.m_cloneFunc;
			m_cloneFuncProvided = other.m_cloneFuncProvided;
			CopyObject(other);
			return *this;
		}

		//move:
		DeepCopyPointer(DeepCopyPointer&& other) :
			m_upObject(std::move(other.m_upObject)),
			m_cloneFunc(std::move(other.m_cloneFunc)),
			m_cloneFuncProvided(std::move(other.m_cloneFuncProvided))
		{ }
		DeepCopyPointer<TObj, TCloneFunc>& operator=(DeepCopyPointer<TObj, TCloneFunc>&& other)
		{
			m_upObject = std::move(other.m_upObject);
			m_cloneFunc = std::move(other.m_cloneFunc);
			m_cloneFuncProvided = std::move(other.m_cloneFuncProvided);
			return *this;
		}

		void Reset(TObj* pObj) { m_upObject.reset(pObj); }

		TObj* Get() const { return m_upObject.get(); }

		TObj& operator*() { return *m_upObject; }
		const TObj& operator*() const { return *m_upObject; }

		TObj* operator->() { return m_upObject.get(); }
		const TObj* operator->() const { return m_upObject.get(); }

		operator bool() const { return m_upObject != nullptr; }

	private:
		std::unique_ptr<TObj> m_upObject;
		TCloneFunc m_cloneFunc;
		bool m_cloneFuncProvided = false;

		void CopyObject(const DeepCopyPointer<TObj, TCloneFunc>& other)
		{
			if (!other)
			{
				m_upObject.reset();
				return;
			}

			if (m_cloneFuncProvided)
			{
				m_upObject.reset(m_cloneFunc(*other));
			}
			else
			{
				if constexpr (!std::is_abstract<TObj>::value)
				{
					m_upObject.reset(new TObj(*other));
				}
			}
		}
	};

	template<typename TObj, typename TCloneFunc>
	bool operator==(const DeepCopyPointer<TObj, TCloneFunc>& a, const DeepCopyPointer<TObj, TCloneFunc>& b)
	{
		return !a && !b ? true :
			!a ? false :
			!b ? false :
			*a == *b;
	}

	template<typename TObj, typename TCloneFunc>
	bool operator!=(const DeepCopyPointer<TObj, TCloneFunc>& a, const DeepCopyPointer<TObj, TCloneFunc>& b)
	{
		return !(a == b);
	}
}