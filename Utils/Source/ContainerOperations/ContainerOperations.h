#pragma once
#include "Utils/Source/Defines.h"
//std
#include <map>
#include <vector>

namespace utils
{
	template<class T>
	void AppendVectors(std::vector<T>& a, const std::vector<T>& b)
	{
		a.insert(a.end(), b.begin(), b.end());
	}

	template<typename TKey, typename TValue>
	std::vector<TValue> MapToVector(const std::map<TKey, TValue>& m)
	{
		std::vector<TValue> v;
		for (const auto& pair : m)
		{
			v.push_back(pair.second);
		}
		return v;
	}

	template<typename T>
	OStream& operator<<(OStream& os, const std::vector<T>& v)
	{
		os << "(";
		for (size_t i = 0; i < v.size(); i++)
		{
			if (0 < i)
			{
				os << ", ";
			}
			os << v[i];
		}
		os << ")";
		return os;
	}
}
